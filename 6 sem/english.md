[toc]


# 1: What is database? (что такое БД)

A database is an organized collection of structured information, or data, typically stored electronically in a computer system.

---

# 2: What is database controlled by? (чем управляются БД)

A database is usually controlled by a database management system (DBMS). (СУБД)

exaples: 

- mysql
- microsoft sql server
- postgreSQL

---

# 3: How is Data within the most common types of databases in operation today  typically modeled ? (Как обычно моделируются данные в наиболее распространенных типах действующих сегодня баз данных)

Data within the most common types of databases in operation today is typically modeled in rows and columns in a series of tables to make processing and data querying efficient.

---

# 4: What are the types of database? (типы БД)

There are many different types of databases. The best database for a specific organization depends on how the organization intends to use the data.

1. relation database (реляционные)

    Relational databases became dominant in the 1980s. Items in a relational database are organized as a set of tables with columns and rows. Relational database technology provides the most efficient and flexible way to access structured information.
2. object-oriented databases (объектно-ориентированные)

    Information in an object-oriented database is represented in the form of objects, as in object-oriented programming.
3. distributed databases

    A distributed database consists of two or more files located in different sites. The database may be stored on multiple computers, located in the same physical location, or scattered over different networks.
4. data warehouses

    A central repository for data, a data warehouse is a type of database specifically designed for fast query and analysis.
5. noSQL databases

    A NoSQL, or nonrelational database, allows unstructured and semistructured data to be stored and manipulated (in contrast to a relational database, which defines how all data inserted into the database must be composed). NoSQL databases grew popular as web applications became more common and more complex.
6. Graph databases

    A graph database stores data in terms of entities and the relationships between entities.
7. OLTP databases
    An OLTP database is a speedy, analytic database designed for large numbers of transactions performed by multiple users.
These are only a few of the several dozen types of databases in use today. Other, less common databases are tailored to very specific scientific, financial, or other functions. In addition to the different database types, changes in technology development approaches and dramatic advances such as the cloud and automation are propelling databases in entirely new directions. Some of the latest databases include
8. Open source databases   
    An open source database system is one whose source code is open source; such databases could be SQL or NoSQL databases.
9. cloud databases
    A cloud database is a collection of data, either structured or unstructured, that resides on a private, public, or hybrid cloud computing platform. There are two types of cloud database models: traditional and database as a service (DBaaS). With DBaaS, administrative tasks and maintenance are performed by a service provider.
10. multimodel database
    Multimodel databases combine different types of database models into a single, integrated back end. This means they can accommodate various data types.
11. document/json database (нереляционная)
    Designed for storing, retrieving, and managing document-oriented information, document databases are a modern way to store data in JSON format rather than rows and columns.
12. self-driving databases (самоуправляемые)
    The newest and most groundbreaking type of database, self-driving databases (also known as autonomous databases) are cloud-based and use machine learning to automate database tuning, security, backups, updates, and other routine management tasks traditionally performed by database administrators. (новые базы данных, основанные на облаке и машинном обучении для автоматизации настройки)

---

# 5: What is big data?

Big data is a combination of structured, semistructured and unstructured data collected by organizations that can be mined for information and used in machine learning projects, predictive modeling and other advanced analytics applications. (комбинация данных, собираемых организациями для изьятия информации для машинного обучения, моделей предсказания, приложений анализа и прочего. еще можно добавить таргетированную рекламу - targeting advertising)

---

# 6: Why is big data important?

Companies use big data in their systems to improve operations, provide better customer service, create personalized marketing campaigns and take other actions that, ultimately, can increase revenue and profits. Businesses that use it effectively hold a potential competitive advantage over those that don't because they're able to make faster and more informed business decisions.
What are the benefits of big data for companies?

6 big data benefits for businesses

1. better customer insight  
2. improved operations
3. more insightful market intelligence
4. agile supply chain management
5. data - driven innovation
6. smarted recommendations and targeting

---

#  7: What are examples of big data?

Big data comes from myriad sources -- some examples are transaction processing systems, customer databases, documents, emails, medical records, internet clickstream logs, mobile apps and social networks. It also includes machine-generated data, such as network and server log files and data from sensors on manufacturing machines, industrial equipment and internet of things devices.

---

# 8: What is software?

Computer software, or simply software, is a part of a computer system that consists of data or computer instructions, in contrast to the physical hardware from which the system is built.

---

# 9: What types of software do you know?

- system software
    serves as a base for application software. System software includes device drivers, operating systems (OSs), compilers, disk formatters, text editors and utilities helping the computer to operate more efficiently. It is also responsible for managing hardware components and providing basic non-task-specific functions. The system software is usually written in C programming language.
- programming software
    is a set of tools to aid developers in writing programs. The various tools available are compilers, linkers, debuggers, interpreters and text editors.
- application software
     is intended to perform certain tasks. Examples of application software include office suites, gaming applications, database systems and educational software. Application software can be a single program or a collection of small programs. This type of software is what consumers most typically think of as "software."

---

# 10: What is innovation?

An innovation is the implementation of a new or significantly improved product, or process, a new marketing method, or a new organizational method in business practices, workplace organization or external relations. This  definition involves  a wide range of possible innovations. An innovation can be more narrowly categorized as the implementation of one or more types of innovations, for instance product and process innovations. 



---

# 11: What is innovation company?

An innovative firm is one that has implemented an innovation during a certain period. (внедрила инновацию в течении определенного периода)

---

# 12: What are the types of innovation according to the Oslo Manual?

product innovations, process innovations, marketing innovations and organizational innovations

---

# 13: What is product innovation?

A product innovation is the introduction of  goods or services that are new or significantly improved with respect to its characteristics or intended uses. This includes significant improvements in technical specifications, components and materials. (развитие технологий)

---

# 14: What is process innovation?

A process innovation is the implementation of a new or significantly improved production or delivery method. This includes significant changes in techniques,
equipment or software. Process innovations can be intended to decrease unit costs of production or delivery, to increase quality, or to produce or deliver new or significantly improved products. (снижение цены)

---

# 15: What is marketing innovation?

A marketing innovation is the implementation of a new marketing method involving significant changes in product design or packaging, product placement,
product promotion or pricing. (улучшение маркетинга)


---

# 16: What is organizational innovation?

An organizational innovation is the implementation of a new organizational method in the firm’s business practices, workplace organization or external relations. Organizational innovations can be intended to increase a firm’s performance by reducing administrative costs or transaction costs, improving workplace satisfaction and thus labour productivity. (изменение организации производства)

---

# 17: What is Organization for Economic Cooperation and Development?

The OECD is a unique forum where the governments of 30 democracies work together to address the economic, social and environmental challenges of globalization.

---

What are the types of innovation according to the American system?

- incremental
- semi-radical
- radical

---

# 18: What is incremental innovation?

Incremental innovation leads to small improvements to existing products and business processes.

(small improvement for product and business)

---

# 19: What is semi-radical innovation?

A semi-radical innovation can provide crucial changes to the competitive environment that an incremental innovation cannot. Semi-radical innovation involves substantial change to either the business model or technology of an organization—but not to both.

(substain change in the business model or technology, but not both)

---

# 20: What is radical innovation?

A radical innovation is a significant(large) change that simultaneously affects both the business model and the technology of a company. Radical innovations usually bring fundamental changes to the competitive environment in an industry. (радикальные инновации в бизнес модели и в технологиях)

---

# 21: What is venture company?

Venture company is a company that deals with applied scientific research, design-and-engineering activity and implementation of technical and technological innovations. (научные исследования, развитие технологий)

---

# 22: What is implementation company?

Implementation company is a company established for adoption and implementation of new technology. (внедрение в деловую практику )

---

# 23: What is explerent company?

Explerent companies are the ones which develop totally new products or create totally new market segments or transform the old ones radically. The explerent strategy is aimed at the radical innovations and establishment of new markets. (создают новые сегменты рынка)

---

# 24: What is patient company?

Patient companies are the ones working on narrow market segments and meeting the demands under the effect of fashion, advertising etc.  The patient strategy presupposes the production of a limited quantity of very specialized items of high quality of limited demand. This helps firms to avoid direct competition with leading corporations in the industry and to reserve stable position on the market of a certain product for a long time. (производство малого числа товара для узкой аудитории)

---

# 25: What is violent company?

Violent companies are the ones, which possess large capital and high level of technology adoption. They are engaged in mass production for wide range of consumers (with average quality demands and average price level). The violent strategy counts on reducing the production costs at the expense of mass output of rather cheap and good quality goods; this allows to retain vast sales markets for a long time. There are three main varieties of violent companies; they operate in the sphere of large, standard production of goods and services deriving their benefits from the saving on the production volume, large-scale scientific research and developed sales network.

(агрессивное распространение)

---

# 26: What is commutant company? 

Commutant companies are small firms of small and medium-sized businesses which are oriented on maximum flexible satisfaction of local market needs and needs of the concrete client. Small firms which as a rule do not have the up-to-date technologies. However, it does not prevent them from forming their market niche. (максимально гибкое удовлетворение потребностей)

---

# 27: What is technopolis?

Technopolis is a form of free economic zone established to activate and accelerate innovation processes that contributes to fast and efficient application of technical  and technological novations.

---

# 28: What is technopark?

Technopark is an organization created to provide companies with equipped production, experiment, informational and infrastructural base. A technopark manages and encourages the information and technology flow between universities, companies and market; facilitates creation and growth of innovative companies; provides high quality venue and capabilities and offers value-added services. 

---

# 29: What is business incubator? 

Business incubator is a company that helps new and startup companies to develop by providing services such as management training or office space. Innovation companies, new equipment manufacturers, consulting companies are business incubator clients.

# доп: как вы планируете работать? (офис, фриланс)

i prefer freelance, work in foreing clients and take slary in currency. And i can use marketplase for passive income



