# №1

![7718cc1d25cd49eec8704d7beb529c52.png](file://D:/Program%20Files%20%28x86%29/joplin/JoplinProfile/resources/feed57c66d4a49ee98a9df2d20a11838.png?t=1609182503290)

```sql
Select model, speed,hd from PC where price < 500
```

# №2

![85e1aca3b43ac70ab36ac0c3e286b843.png](file://D:/Program%20Files%20%28x86%29/joplin/JoplinProfile/resources/b74446ed24344989a13f3830fa88b15c.png?t=1609182493475)

```sql
Select maker from Product where type = 'Printer' group by maker
```

# №3

![04e33ba4d7dee392b516c592a0928217.png](file://D:/Program%20Files%20%28x86%29/joplin/JoplinProfile/resources/3b5ca870fecd4cf1b9be0b761477be40.png?t=1609182520661)

```sql
Select model, ram,screen from Laptop where price > 1000
```

# №4

![4993eb09a703b62719990448edb2386f.png](file://D:/Program%20Files%20%28x86%29/joplin/JoplinProfile/resources/a0d92fb2a1e74bd2bd2922a620c17a6c.png?t=1609182573622)

```sql
Select * from printer where color = 'y'
```

# №5

![2e4d1a702c47a853788f0a85cf37f888.png](file://D:/Program%20Files%20%28x86%29/joplin/JoplinProfile/resources/1f8b7c10b31048bb9ac6f647d8c77085.png?t=1609182600232)

```sql
Select model, speed, hd from PC where (cd = '12x' or cd = '24x') and price < 600
```



# 52 

```sql
Select distinct name from (
select case 
WHEN (type = 'bb' or type is null)
and (country = 'japan' or country is null)
and (numGuns >= 9 or numGuns is null)
and (bore < 19 or bore is null)
and (displacement <= 65000 or displacement is null)
then name end name 
from ( 
select name, type, country, numGuns, bore, displacement from classes join 
ships on classes.class = ships.class
) this_table
) this_table_2 where name is not null


```

# 53

```sql
Select cast(avg(cast(numGuns as decimal (6,2))) as decimal (6,2)) avgNum from (
select name, type, numGuns from classes join ships on classes.class = 
ships.class where type = 'bb'
union
select ship, type, numGuns from classes join outcomes on ship = class where 
type = 'bb' 
) this_table

```


# 54

```sql
Select cast(avg(cast(numGuns as decimal(6,2))) as decimal(6,2)) avgNum from (
select name, type, numGuns from classes join ships on classes.class = 
ships.class where type = 'bb'
union 
select ship, type, numGuns from classes join outcomes on ship = class where type = 'bb' 
) this_table

```

# 55

```sql
Select class, min(launched) from (
select classes.class, launched from classes left join ships on classes.class = ships.class
) this_table group by class
```

# 56

```sql
Select classes.class, count(name) from classes left join (
select classes.class, name from classes join ships on classes.class = ships.class where name in (select ship from outcomes where result = 'sunk')
union 
select class, ship from classes join outcomes on class = ship where result = 'sunk'
) this_table on classes.class = this_table.class group by classes.class

```

# 57

```sql
Select class, count(name) from (
select classes.class, name from classes join ships on classes.class = ships.class where name in (select ship from outcomes where result = 'sunk' )
union 
select class, ship from classes join outcomes on class = ship where result = 'sunk'
) this_table where class in (
select class from (
select classes.class, name from classes join ships on classes.class = ships.class
union
select class, ship from classes join outcomes on class = ship 
) this_table_2 group by class having count(*) >= 3
) group by class 

```

# 58

```sql

```

# 59

```sql

```

# 60


```sql

```

# 61


```sql

```

# 62


```sql

```

# 63


```sql

```

# 64


```sql

```

# 65


```sql

```

# 66


```sql

```

# 67


```sql

```

# 68


```sql

```

# 69


```sql

```

# 70


```sql

```


# расчет сделанных задач

05/01/21 19:09
1. - [x] 38
2. - [x] 39
3. - [x] 40
4. - [x] 41
5. - [x] 42
6. - [x] 43
7. - [x] 44 
8. - [x] 45
9. - [x] 46
10. - [x] 47
11. - [x] 48
12. - [x] 49 
13. - [x] 50 
14. - [x] 51 
15. - [x] 52 
16. - [x] 53
17. - [x] 54
18. - [x] 55
19. - [x] 56 
20. - [x] 57
21. - [ ] 58 
22. - [ ] 59
23. - [ ] 60
24. - [ ] 61
25. - [ ] 62