# код


подключение каталога для загрузки файлов

```php
 $path = GetCWD() . "/loadfiles";
```
обработка ошибок

```php
die();
```


проверка на наличие файла

```php
if (file_exists('example.jpeg')) {
        echo 'такой файл уже есть';
    }
    else {
        echo 'файла нет';
    }
```



полный код

```php
$path = GetCWD() . "/loadfiles";
    

    if (!file_exists($path))
        die("<b> Пожалуйста, создайте папку <font color = red>" . $path . "</font> и<a href = &#63;> повторите попытку загрузить файл</a>.</b>");

    if (empty($_FILES['UserFile']['tmp_name'])) echo "<form method = post enctype=multipart/form-data>Выберите файл: <input type=file name = UserFile><input type=submit value = Отправить></form>";

    else if (!is_uploaded_file($_FILES['UserFile']['tmp_name']))
        die("<b><font color = red> Файл не был загружен! Попробуйте <a href=&#63;>повторить попытку</a>!</font></b>");

    else {

        if (@!copy($_FILES['UserFile']['tmp_name'], $path . chr(47) . $_FILES['UserFile']['name']))

            die("<b><font color=red>Файл не был загружен! Попробуйте <a href=&#63;>повторить попытку</a>!</font></b>");

        else echo "<center> <b> Файл \" <font color = red>" .
            $_FILES['UserFile']['name'] . "\"</font> успешно загружен на сервер!</font></b></center>" . "<hr>" .
            "Тип файла: <b>" . $_FILES['UserFile']['type'] . "</b><br>" . "Размер файла: <b>" .
            round($_FILES['UserFile']['size'] / 1024, 2) .
            " кб.</b>" . "<hr> <center> <a href = &#63;>Загрузить ещё один файл! </a></center>";
    }

    if (file_exists('example.jpeg')) {
        echo 'такой файл уже есть';
    }
    else {
        echo 'файла нет';
    }
```








# загрузка на сервер


![c82613ed85161787cc274153a38ca7ab.png](../../../_resources/c82613ed85161787cc274153a38ca7ab.png)

![eeaaab0a53db650d3ebb70ecf35119f0.png](../../../_resources/eeaaab0a53db650d3ebb70ecf35119f0.png)

![161c5c726e749ad9cac737f93875e76c.png](../../../_resources/161c5c726e749ad9cac737f93875e76c.png)