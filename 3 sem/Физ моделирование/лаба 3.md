# видео

https://youtu.be/G7t9TgC_loE



# задание 1

Объект находится в начальной точке. Через промежуток времени t1 объект получает ускорение a, изменяющееся по закону: a = A+B*t. Определить путь пройденный объектом за время t2, величину скорости и ускорения в времени момент t3 (t1<t3<t2). Движение вдоль 1 координатной оси

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_1 : MonoBehaviour
{
    public float A, B, t1, t2, t3;
    float acceleration;
    float speed = 0;
    float counter = 0;
    float path;
    bool showedT2, showedT3 = false;

    void Update()
    {
        counter += Time.deltaTime;
        acceleration = A + B * (counter - t1);

        if (counter > t1) Acceleration();
        if (!showedT2 && counter > t2)
        {
            print("Path:  " + t2 + " sec: " + path);
            showedT2 = true;
        }
        if (!showedT3 && counter > t3)
        {
            print("Speed:  " + t3 + " sec: " + speed + " rate: " + acceleration);
            showedT3 = true;
        }
    }

    void Acceleration()
    {
        speed += acceleration * (counter - t1);
        transform.Translate(speed * Time.deltaTime, 0, 0);
        path = speed * (Time.unscaledTime - t1);
    }
}

```



# задание 2

Объект находится в начальной точке. Через промежуток времени t1 объект получает ускорение aх, изменяющееся по закону: a = A+B*t; и ay, изменяющееся по закону: a=C+D*t. Определить путь пройденный объектом за время t2, величину скорости и ускорения в момент времениt3 (t1<t3<t2). Движение    вдоль 2-х координатных осей

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_2 : MonoBehaviour
{
    public float A, B, C, D, t1, t2, t3;
    float RateX, RateY;
    float Rate;
    float speedX, speedY = 0;
    float speed;
    float counter = 0;
    float path;
    bool message_timeT2, message_timeT3 = false;


    void Update()
    {
        counter += Time.deltaTime;
        RateX = A + B * (counter - t1);
        RateY = C + D * (counter - t1);

        if (counter > t1) Acceleration();
        
        if (!message_timeT2 && counter > t2)
        {
            print("Path: " + t2 + " seconds: " + path);
            message_timeT2 = true;
        }
        if (!message_timeT3 && counter > t3)
        {
            print("Speed: " + t3 + " seconds: " + speed + " rate: " + Rate);
            message_timeT3 = true;
        }
    }

    void Acceleration()
    {
        speedX += RateX * (counter - t1);
        speedY += RateY * (counter - t1);

        transform.Translate(speedX * Time.deltaTime, speedY * Time.deltaTime, 0);
        speed = Mathf.Sqrt(Mathf.Pow(speedX, 2) + Mathf.Pow(speedY, 2));
        Rate = Mathf.Sqrt(Mathf.Pow(RateX, 2) + Mathf.Pow(RateY, 2));
        path = speed * (Time.unscaledTime - t1);
    }
}

```

