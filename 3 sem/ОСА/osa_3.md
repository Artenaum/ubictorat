# Цель

Получить практические навыки в создании односвязных и двусвязных списков

# Постановка задачи

Используя результаты предшествующей лабораторной работы, в соответствии с заданной

структурной единицей хранения информации, разработать алгоритмы

# Алгоритм

- Добавление элемента в конец списка 
- Добавление элемента в начало списка 
- Удаление конечного элемента списка 
- Удаление начального элемента списка
- Поиск элемента по заданному значению поля структуры 
- Добавление элемента после найденного
- Удаление найденного элемента

# Код

```c++
#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;


struct tom {
public:
    int tm_min,
    tm_hour,
    tm_day, 
    tm_month;
};

struct movie {
public:
    tom time; 
    string name; 
    int price, 
        viewers; 
    bool is_active = false;
};

struct single_list {
    single_list* next;
    movie value;
};

struct double_list {
    struct double_list* previous;
    struct double_list* next;
    movie value;
};


///////////////////////////////////////////////////////////////////////

movie fill() { 
    movie temp;
    tom temp_time;
    string temp_name;
    int temp_price, temp_viewers; 
        cout << "Имя:" << endl;
    cin >> temp_name;
    cout << "минута:" << endl;
    cin >> temp_time.tm_min;
    cout << "час:" << endl;
    cin >> temp_time.tm_hour;
    cout << "день:" << endl;
    cin >> temp_time.tm_day;
    cout << "месяц:" << endl;
    cin >> temp_time.tm_month;
    cout << "зрители:" << endl;
    cin >> temp_viewers;
    cout << "цена:" << endl;
    cin >> temp_price;
    temp.name = temp_name;
    temp.time = temp_time;
    temp.price = temp_price;
    temp.viewers = temp_viewers;
    temp.is_active = true;
    return temp;
}

///////////////////////////////////////////////////////////////////////



single_list* create_single() {
    movie a = fill();
    struct single_list* temp;
    temp = new single_list;
    temp->value = a;
    temp->next = NULL; 
    return(temp);
}

///////////////////////////////////////////////////////////////////////


double_list* create_double() {
    movie a = fill();
    struct double_list* temp;
    temp = new double_list;
    temp->value = a;
    temp->next = NULL; 
    temp->previous = NULL;
    return(temp);
}

///////////////////////////////////////////////////////////////////////



single_list* add_end(single_list* first, movie a = fill()) {
    single_list* temp = first, * create;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    create = new single_list;
    create->value = a;
    create->next = NULL;
    temp->next = create;
    return(create);
}

///////////////////////////////////////////////////////////////////////


double_list* add_end(double_list* first, movie a = fill()) {
    double_list* temp = first, * create;
    while (temp->next != NULL) {
        temp = temp->next;
    }
    create = new double_list;
    create->value = a;
    create->next = NULL;
    create->previous = temp;
    temp->next = create;
    return(create);
}

///////////////////////////////////////////////////////////////////////



single_list* add_start(single_list* first, movie a = fill()) {
    single_list* temp = first, * create;
    create = new single_list;
    create->value = a;
    create->next = first;
    return(create);
}

///////////////////////////////////////////////////////////////////////


double_list* add_start(double_list* first, movie a = fill()) {
    double_list* temp = first, * create;
    create = new double_list;
    create->value = a;
    create->next = first;
    create->previous = NULL;
    temp->previous = create;
    return(create);
}

///////////////////////////////////////////////////////////////////////



void delete_end(single_list* first) {
    bool flag = false;
    single_list* temp = first, * previous;
    do {
        previous = temp;
        temp = temp->next;
        flag = true;
    } while (temp->next != NULL); previous->next = NULL;
    delete temp;
}

///////////////////////////////////////////////////////////////////////



void delete_end(double_list* first) {
    bool flag = false;
    double_list* temp = first, * previous;
    do {
        previous = temp;
        temp = temp->next;
        flag = true;
    } while (temp->next != NULL); previous->next = NULL;
    delete temp;
}

///////////////////////////////////////////////////////////////////////



single_list* delete_begin(single_list* first)
{
    single_list* temp = first->next;
    delete first;
    return(temp);
}

///////////////////////////////////////////////////////////////////////



double_list* delete_begin(double_list* first)
{
    double_list* temp = first->next; temp->previous =
        NULL;
    delete first;
    return(temp);
}

///////////////////////////////////////////////////////////////////////



single_list* search(single_list* first) {
     int search;
    int closeprice, targetprice, similarities = 0,
        newsimilarities = 0;
    string closename, targetname;
    bool flag = false;
    single_list* temp = first, * index = first;
    cout << "Search by 1-price, 2-name" << endl; cin >>
        search;
    if (search == 1) {
        cin >> targetprice;
    }
    else {
        cin >> targetname; 
    }
    while (true) {
        if (search == 1) {
            if (flag) {
                if (abs(temp->value.price - targetprice) <
                    abs(closeprice - targetprice)) {
                    closeprice = temp->value.price;
                    index = temp;
                }
            }
            else {
                index = temp;
                closeprice = temp->value.price;
                flag = true;
            }
        }
        else {
            if (flag) {
                int sizetarget = targetname.length();
                int sizecurrent = temp->value.name.length();
                for (int j = 0; j < min(sizetarget, sizecurrent); j ++) {
                    if (targetname[j] == temp->value.name[j]) {
                        newsimilarities += 1;
                    }
                }
                if (newsimilarities > similarities) {
                    index = temp;
                    closename = temp->value.name;
                    similarities = newsimilarities;
                }
                newsimilarities = 0;
            }
            else {
                index = temp;
                closename = temp->value.name;
                flag = true;
                int sizetarget = targetname.length();
                int sizecurrent = temp->value.name.length();
                for (int j = 0; j < min(sizetarget, sizecurrent); j++) {
                    if (targetname[j] == temp->value.name[j]) {
                        similarities += 1;
                    }
                }
            }
        }
        if (temp->next == NULL) {
            break;
        }
        else {
            temp = temp->next;
        }
    }
    return index;
}



///////////////////////////////////////////////////////////////////////




double_list* search(double_list* first) { 
    int search;
    int closeprice, targetprice, similarities = 0,
        newsimilarities = 0;
    string closename, targetname;
    bool flag = false;
    double_list* temp = first, * index = first; cout <<
        "поиск: цена, название" << endl; cin >> search;
    if (search == 1) {
        cin >> targetprice;
    }
    else {
        cin >> targetname; 
    }
    while (true) {
        if (search == 1) {
            if (flag) {
                if (abs(temp->value.price - targetprice) <
                    abs(closeprice - targetprice)) {
                    closeprice =
                        temp->value.price;
                    index = temp;
                }
            }
            else {
                index = temp;
                closeprice = temp->value.price;
                flag = true;
            }
        }
        else {
            if (flag) {
                int sizetarget = targetname.length();
                int sizecurrent = temp->value.name.length();
                for (int j = 0; j < min(sizetarget, sizecurrent); j++) {
                    if (targetname[j] == temp->value.name[j]) {
                        newsimilarities += 1;
                    }
                }
                if (newsimilarities > similarities) {
                    index = temp;
                    closename = temp->value.name;
                    similarities = newsimilarities;
                }
                newsimilarities = 0;
            }
            else {
                index = temp;
                closename = temp->value.name;
                flag = true;
                int sizetarget = targetname.length();
                int sizecurrent = temp->value.name.length();
                for (int j = 0; j < min(sizetarget, sizecurrent); j++) {
                    if (targetname[j] == temp->value.name[j]) {
                        similarities += 1;
                    }
                }
            }
        }
        if (temp->next == NULL) {
            break;
        }
        else {
            temp = temp->next;
        }
    }
    return index;
}

///////////////////////////////////////////////////////////////////////



single_list* search_add(single_list* first, movie elem) {
    single_list* temp = search(first);
    if (temp->next == NULL) {
        return(add_end(temp, elem));
    }
    else if (temp->next != NULL) {
        single_list* after = temp->next, * adding;
        adding = new single_list;
        adding->value = elem;
        adding->next = after;
        temp->next = adding;
        return(adding);
    }
}


///////////////////////////////////////////////////////////////////////



double_list* search_add(double_list* first, movie elem) {
    double_list* temp = search(first);
    if (temp->next == NULL) {
        return(add_end(temp, elem));
    }
    else if (temp->next != NULL) {
        double_list* after = temp->next, * adding;
        adding = new double_list;
        adding->value = elem;
        adding->next = after;
        temp->next = adding;
        return(adding);
    }
}


///////////////////////////////////////////////////////////////////////




single_list* search_delete(single_list* first) {
    single_list*
        temp = search(first), * finding = first, * previous = first;
    if (temp == first) {
        finding = temp->next;
        delete temp;
        return finding;
    }
    else if (temp->next != NULL) {
        while (finding != temp) {
            previous = finding;
            finding = finding->next;
        }
        previous->next = finding->next;
        delete finding;
        return first;
    }
    else if (temp->next == NULL) {
        delete_end(first);
        return first;
    }
}


///////////////////////////////////////////////////////////////////////



double_list* search_delete(double_list* first) {
    double_list* temp = search(first), * finding =
        first, * previous = first; if (temp == first) {
        finding = temp->next;
        delete temp;
        return finding;
    }
        else if (temp->next != NULL) {
        while (finding != temp) {
            previous = finding;
            finding = finding->next;
        }
        previous->next = finding->next;
        delete finding;
        return first;
    }
        else if (temp->next == NULL) {
        delete_end(first);
        return first;
    }
}


///////////////////////////////////////////////////////////////////////



void show(single_list* show) {
    cout << "Name:" << show->value.name << endl;
    cout << "Date: " << show->value.time.tm_hour << ':'
        << show->value.time.tm_min <<
        ' ' << show->value.time.tm_day << '.' << show -> value.time.tm_month <<
        endl;
    cout << "Viewers: " << show -> value.viewers << endl;
    cout << "Price: " << show->value.price << endl;
} 

///////////////////////////////////////////////////////////////////////



void show_all(single_list* first) {
    single_list* current = first;
    while (true) {
        show(current);
        if (current->next != NULL) {
            current = current->next;
        }
        else {
            break;
        }
    }
}


///////////////////////////////////////////////////////////////////////


int main() {
    cout << "enter head element value: " << endl;
    single_list* single = create_single(); movie temp;
    int check = 0, choise;
    cout << "1: добавить элемент в конец " <<
        "2: добавить элемент в начало" <<
        "3: удалить первый элемент " <<
        "4: удалить последний элемент " <<
        "5: найти элемент и добавить еще один " <<
        "6: найти и удалить " <<
        "7: показать элементы " << " - 8 - exit" << endl;
   
    while (true) {
        cin >> check;
        if (check == 1) {
            add_end(single);
        }
        else if (check == 2) {
            single = add_start(single);
        }
        else if (check == 3) {
            single = delete_begin(single);
        }
        else if (check == 4) {
            delete_end(single);
        }
        else if (check == 5) {
            search_add(single, fill());
        }
        else if (check == 6) {
            single = search_delete(single);
        }
        else if (check == 7) {
            show_all(single);
        }
        else if (check == 8) {
            break;
        }
    }
    return 0;
}

```



# Результат



![image-20210129193834643](C:\Users\Александр\AppData\Roaming\Typora\typora-user-images\image-20210129193834643.png)

