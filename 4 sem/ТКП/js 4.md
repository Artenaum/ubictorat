# задание 002

Создать функции двух аргументов:
сложения,
вычитания,
умножения,
деления.
В каждой функции предусмотреть вывод результата в окне браузера в формате:
a + b = 8
a - b = 3
a * b = 16
a / b = 2
Попытка деления на 0. 

```js
let a = 8;
let b = 4; 
let res = document.getElementById('result');
//  сложение
function sum() {
	res.innerHTML += 'сумма: ' + a + b + '<br>';
}
// вычитание
function sub() {
	res.innerHTML += 'вычитание: ' + (a - b) + '<br>';
}
// умножение

function mult() {
	res.innerHTML += 'умножение: ' + a * b + '<br>';
}
// деление
function div() {
	if (b == 0) {
		res.innerHTML += 'деление на ноль';
	}
	else {
		res.innerHTML += 'деление: ' + a / b + '<br>';
	}
}

sum();
sub();
mult();
div();
```




![6690ce7f2307d5f7a6be2a834ffdbcd2.png](../../../_resources/6690ce7f2307d5f7a6be2a834ffdbcd2.png)


# задание 003

Создать функцию одного числового аргумента, решающую следующие задачи:
    1. Вывод в окно браузера сообщения о знаке числа;
    2. Проверка на кратность 2 или 3 или 5 или 9 и в случае кратности хотябы одному из означенных чисел, вывод в окно браузера сообщения: "Число делится на 2 или 5 или 3 или 6 или 9 без остатка";
    3.  Проверка является ли параметр функции простым числом и вывод в кне браузера соответствующей информации.

```js
let res = document.getElementById('result');
res.innerHTML = 'переменная a = 8' + '<br>';

function f() {
    let a = 8;
    if (a > 0) {
        res.innerHTML += 'знак +' + '<br>';
    }
    else {
        res.innerHTML += 'знак -' + '<br>';
    }
    if (a % 2 || a % 3 || a % 5 || a % 9) {
        res.innerHTML += 'число дел' + '<br>';
    }

    if(a % a == 0 && a %1 == 0 && a % 2 != 0 && a % 3 != 0) 
    res.innerHTML = "простое <br/>" 
    else if (a == 2 || a == 3) 
    res.innerHTML += "число простое <br/>"
    else 
    res.innerHTML += "число составное <br/>"; 
}

f();

f();
```


![cd1e87c8e5d08f6ce033d3b75e1ba76a.png](../../../_resources/cd1e87c8e5d08f6ce033d3b75e1ba76a.png)

# задание 004

```js

    let stek = [];

    let turn = [];

    let res = document.getElementById('result');


    // добавление элемента в стек
    function stek_add() {
        stek.push(1 + ' ');    
        stek.push(2 + ' ');
        stek.push(3 + ' ');
        stek.push(4 + ' ');
        stek.push(5 + ' ');

        for (let i = 0; i < 5; i++) {
            res.innerHTML += stek[i];
        }
        res.innerHTML += '<br>';
    }
    // извлечение элемента из стека
    function stek_rem() {
        stek.pop();
        for (let i = 0; i < 5; i++) {
            res.innerHTML += stek[i];
        }
        res.innerHTML += '<br>';
    }
    // добавление элемента в очередь
    function turn_add() {

        turn.unshift(2 + '\n', 3 + '\n', 5 + '\n', 4326 + '\n', -352);
        for (i = 0; i < 5; i++) {
            res.innerHTML += turn[i];
        }
        res.innerHTML += '<br>';
    }
    // извлечение элемента из очереди
    function turn_rem() {
        turn.shift();
        for (i = 0; i < 5; i++) {
            res.innerHTML += turn[i];
        }
        res.innerHTML += '<br>';
    }


    stek_add();
    stek_rem();
    turn_add();
    turn_rem();

```




![7bc7187fd3ed50ee571ad1e9fdcaa4b1.png](../../../_resources/7bc7187fd3ed50ee571ad1e9fdcaa4b1.png)

