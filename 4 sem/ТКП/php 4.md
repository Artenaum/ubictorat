# код

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>




    <?php

    // подключание к inputам со значениями
    $first = $_POST['first'];
    $second = $_POST['second'];
    $action = $_POST['action'];
    // объявление переменной результата
    $result = '';



    // подключение к меню с действиями
    switch ($action) {
            // сумма
        case "Add":
            $result = $first + $second;
            break;
            // вычитание
        case "Subtract":
            $result = $first - $second;
            break;
            // умножение
        case "Multiply":
            $result = $first * $second;
            break;
            // деление
        case "Divide":
            // проверка на наличие делителя
            if (!$second) {
                exit("Second number not entered or equal to zero");
            } else {
                $result = $first / $second;
            }
            break;
            // возведение в степень
        case "Exponentiation":
            $result = pow($first, $second);
            break;
            // процент от числа
        case "Persent":
            $result = $first % $second;
            break;
            // синус
        case "sin":
            $result = sin(deg2rad($first));
            break;
            // косинус
        case "cos":
            $result = cos(deg2rad($first));
            break;
            // тангенс
        case "tg":
            $result = tan(deg2rad($first));
            break;
            // котангенс
        case "ctg":
            $result = 1 / tan(deg2rad($first));
            break;
    }
    ?>


    <form action="calculator.php" method="post">
        <table border="1" cellspacing="2" cellpadding="2">
            <tr>
                <td>First number: </td>
                <td><input size="14" type="text" name='first' value="<?php echo $first; ?>"></td>
            </tr>
            <tr>
                <td>Second number: </td>
                <td><input size="14" type="text" name='second' value="<?php echo $second; ?>"></td>
            </tr>
            <tr>
                <td>
                    <select name="action" size="1">
                        <option value="Add">Add</option>
                        <option value="Subtract">Substract</option>
                        <option value="Multiply">Multiply</option>
                        <option value="Divide">Divide</option>
                        <option value="Exponentiation">Exponentiation</option>
                        <option value="Persent">Percentage of the number</option>
                        <option value="sin">sin</option>
                        <option value="cos">cos</option>
                        <option value="tg">tg</option>
                        <option value="ctg">ctg</option>
                    </select>
                </td>
                <td><input type="submit" value="Выполнить" name='but'>
                </td>
            </tr>
            <tr>
                <td>Result:</td>
                <td>
                    <input name="result" value="<?php echo $result; ?>">
                </td>
            </tr>
        </table>
    </form>



</body>

</html>
```


# скрины

![20ab5e90b44fe5d2b281fa4bf807e1a6.png](../../../_resources/20ab5e90b44fe5d2b281fa4bf807e1a6.png)

![49255617ed98b5129756bce998f02f22.png](../../../_resources/49255617ed98b5129756bce998f02f22.png)

сумма

![372a3fac139511e127072ad27653dde6.png](../../../_resources/372a3fac139511e127072ad27653dde6.png)

разность

![3c129244528dce1dbf4af879ba0f7fff.png](../../../_resources/3c129244528dce1dbf4af879ba0f7fff.png)

умножение

![22563bee85b744270cb49a5bec753be9.png](../../../_resources/22563bee85b744270cb49a5bec753be9.png)

деление

![342ff0eee8ea24e4beda496137de4e24.png](../../../_resources/342ff0eee8ea24e4beda496137de4e24.png)

степень

![701aa16773d94a313259f2fde131673e.png](../../../_resources/701aa16773d94a313259f2fde131673e.png)

процент

![e3c8ec1ccd32e5afb50a42304d86043c.png](../../../_resources/e3c8ec1ccd32e5afb50a42304d86043c.png)

синус

![0d68b5a4cebca102d9da92a9fb53b073.png](../../../_resources/0d68b5a4cebca102d9da92a9fb53b073.png)

косинус

![a43f97b42666b62e3978c86e6914aebc.png](../../../_resources/a43f97b42666b62e3978c86e6914aebc.png)

тангенс

![cbcd6b551d9910a82e7c8a92914cbee8.png](../../../_resources/cbcd6b551d9910a82e7c8a92914cbee8.png)

котангенс

![b62a41b7036d699fd6c3c8c41f84140d.png](../../../_resources/b62a41b7036d699fd6c3c8c41f84140d.png)