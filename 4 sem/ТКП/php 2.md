# страница со скриптом

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
    body {
        background: #777;
        color: #fff;
    }
</style>

<body>
    <?php
    echo $message . ", " . $name;
    ?>
</body>

</html>
```







# код страницы с формой

сделать список 

```html
	<form name="myform" method="post" action="script_php_2.php">

        <br>Сообщение:<br>
        <input type="text" name="message">

        <br>Имя:<br>

        <select name="name">
            <option>Николай</option>
            <option>Абдулбай</option>
            <option>Анатолий вадимаич</option>
        </select>

        <br><br>
        <input type="submit" value="Обработать данные на сервере">
    </form>
```

![f87eaf37481a7b153775bdbbd2dd38a5.png](../../../_resources/f87eaf37481a7b153775bdbbd2dd38a5.png)



# задание 1 - сделать скрипт, выводящий приветствие с именем

```php
echo $_POST["message"] . ', ' . $_POST["name"] . '!';
```


![89606acbe208181898c497acebec0386.png](../../../_resources/89606acbe208181898c497acebec0386.png)



# задание 2 - radiobutton

```php
echo $_POST["message"] . ', ' .  $_POST['name'] . '!';
```

```php
<form method="POST" action="script_php_2.php">

        <br>Сообщение:<br>
        <input type="text" name="message">

        <br>Имя:<br>
        <div class="wrapper">
            <div class="wrapper_div"><input type="radio" value="Иосиф" name="name" /> Иосиф</div>
            <div class="wrapper_div"><input type="radio" value="Дмитрий" name="name" /> Дмитрий</div>
            <div class="wrapper_div"><input type="radio" value="Магамед" name="name" /> 		 Магамед</div>
            <div class="wrapper_div"><input type="radio" value="Боярин" name="name" /> Боярин</div>
        </div>

        <br><br>
        <input type="submit" value="Обработать данные на сервере">
        
    </form>
```


![10b674927cffcec8f58318686e6f07a5.png](../../../_resources/10b674927cffcec8f58318686e6f07a5.png)

![7022c0993c4c1fe88619bcad872263d7.png](../../../_resources/7022c0993c4c1fe88619bcad872263d7.png)

# задание 3 - изменить фон


```html
   <form method="POST">
        <input type="text" name="color">
        <input type="submit" onclick="btn" name="btn_color" value='изменить цвет'>
    </form>




    <?php
    echo $_POST["message"] . ', ' .  $_POST['name'] . '!';

    $blue = "<body bgcolor='blue'>";
    $red = "<body bgcolor='red'>";
    $color = $_POST['color'];

    if (isset($_POST['btn_color'])) {
        if ($color == 'red') {
             echo $red;
        }
        if ($color == 'blue') {
            echo $blue;
        }
    }
    ?>
```




![3a590417bb318feb1444adaddfacb80a.png](../../../_resources/3a590417bb318feb1444adaddfacb80a.png)

![dbd771eabe4c46310469826a4ee7c100.png](../../../_resources/dbd771eabe4c46310469826a4ee7c100.png)