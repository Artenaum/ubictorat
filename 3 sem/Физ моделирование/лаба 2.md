# видео 

https://youtu.be/N504CVrJVlo

# Задание 1

Объект вращается вокруг неподвижного центра равноускоренно. Расстояние от тела до центра – R. Известна частота вращения ν, с - 1. Определить: число оборотов за время t, угловую скорость объекта

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class script_1 : MonoBehaviour {
    public float U, R, a;
    public float angle, speed, speed_0, time, timer;
    public int circle;
    public Vector3 center;
    


void Start() {
    speed_0 = 2 * Mathf.PI * U;
}

void FixedUpdate() {
    if(time < timer) {
        speed = speed_0 + a * time;
        transform.position = new Vector3 (center.x + Mathf.Cos(angle * Mathf.PI / 180) * R, center.y + Mathf.Sin(angle * Mathf.PI / 180) * R, center.z);
        angle += speed * Time.deltaTime * 180 / Mathf.PI;
        time += Time.deltaTime;
        
        if(angle >= 360) {
            angle = 0;
            circle++;
            }
    }

    print("Скорость: "+ System.Math.Round(speed, 2));
    print("Обороты: " + circle);
    
    }
}
```



# Задание 2

Объект вращается вокруг неподвижного центра равноускоренно. Расстояние от тела до центра –R. Известна частота вращения ν, с-1. Определить: угол поворота и путь, пройденный объектом, за время t

## скрипт

```c#
using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class script_2: MonoBehaviour { 
    
    public float U, R, S, a, S1;
    public float angle, time, timer; 
    public float speed, speed_0;
    public Vector3 center;
    
    void Start() { 
        speed_0 = 2 * Mathf.PI * U;
    }

    void FixedUpdate() {
        
        if (time < timer) {
            speed = speed_0 + a * time;
            transform.position = new Vector3(center.x + Mathf.Cos(angle * Mathf.PI / 180) * R,center.y + Mathf.Sin(angle * Mathf.PI / 180) * R, center.z);
            angle += speed * Time.deltaTime * 180 / Mathf.PI;
            time += Time.deltaTime;
            S = 2 * Mathf.PI * R * (angle / 360);
            
            if (angle >= 360) { 
                S1 += 2 * Mathf.PI * R;
                angle = 0;
            
            }
        }
        print("Угол: "+ (int)angle);
        print("Путь: "+ System.Math.Round(S+S1, 2));
    }
}
```



# Задание 3

Объект вращается вокруг неподвижного центра равноускоренно. Расстояние от тела до центра – R. Известна частота вращения ν, с - 1. Определить: угол поворота, координату и линейную скорость объекта в момент времени t

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_3 : MonoBehaviour {

    public float U, R, a, V;
    public float angle, time, timer;
    public float speed, speed_0;
    public Vector3 center;
    
    void Start()
    {
       speed_0 = 2 * Mathf.PI * U; 
    }

    
    void Update()
    {
        if (time < timer) {
            U = speed / (2 * Mathf.PI * R);
            speed = speed_0 + a * time;
            transform.position = new Vector3(center.x + Mathf.Cos(angle * Mathf.PI / 180) * R, center.y + Mathf.Sin(angle * Mathf.PI / 180) * R, center.z);
            angle += speed * Time.deltaTime * 180 / Mathf.PI;
            time += Time.deltaTime;
            if (angle >= 360) {
                angle = 0;
            }
            V = 2 * Mathf.PI * R * U;
        }

        print("Угол: " + (int)angle);
        print("Координаты: x: " + System.Math.Round(transform.position.x, 2) + "y: " + System.Math.Round(transform.position.y, 2));
        print("Линейная скорость: " + V);
    }
}

```

