1

Today I will tell you about one of the types of Internet crimes - the Trojan virus.

2 

Trojan, a virus that infects individual files, destroying or modifying it in the process, moving to other files. named after the Trojan horse myth

3

the virus disguises itself as another program or email attachment	, most often free, downloaded from unverified sources. as soon as the user launches and installs the program, it takes effect



the virus can behave as it pleases. for example, attackers can use an infected machine to attack other devices, or 

4

These Trojans are archives specially formed in such a way as to cause the archivers to behave abnormally when trying to unpack data - freezing or significantly slowing down the computer or filling the disk with a large amount of "empty" data.

5		

backdoor	

provides hackers with remote access to the device, attackers can remotely perform any actions on it, including sending, receiving, opening and deleting files, displaying data and rebooting

