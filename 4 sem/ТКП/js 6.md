# задание 006

Для объектно-ориентированной программной модели стека разработать пользовательский графический интерфейс. 

```html
<canvas width="500px" height="500px"></canvas>

<div id="output"></div>

<script type="text/javascript">

let output = document.getElementById('output');

const colors = ["#0076c0"];

let randomColor = () => {
    return colors[Engine.irandom(colors.length - 1)]
};


class IDrawable {
    constructor() {
        UI.drawable.push(this);
    }
    draw() {}
}

class Stack extends IDrawable {
    constructor() {
        super();
        this.elems = [];
    }

    add(e) {
        this.elems.unshift(e);
    }

    get() {
        if (this.elems.length == 0) {
            output.innerHTML = 'пустой';
        }
        else {
            return this.elems.shift();
        }
    }

    draw() {
        for (let i = 0; i < this.elems.length; i++) {
            UI.drawRectWithText(25+ 50*(this.elems.length - 1 - i), 50, 50, 50, this.elems[i]);
        }
        UI.drawPointer(0, 0, "#f00");
    }
}

class UI {

    static init() {
        this.canvas = document.getElementsByTagName('canvas')[0];
        this.ctx = this.canvas.getContext('2d');
        this.drawable = [];


        this.canvas.onclick = (ev) => {
            let mouse = this.getMousePos(ev);
            let blockWidth = UI.canvas.width / 3 - 10;
            let blockHeight = 50;

            function inBounds(minX, minY, maxX, maxY) {
                return (mouse.x > minX && mouse.x < maxX ) && (mouse.y > minY && mouse.y < maxY);
            }

            if (inBounds(120 - blockWidth / 2, 100 - blockHeight,130 + blockWidth / 2,100)) {
                Engine.stack.add({value:Engine.next.text, color:Engine.next.color});
                Engine.next.next.text = Engine.irandom(9);
                UI.redraw();
            }

            if (inBounds(120 - blockWidth / 2, 150 - blockHeight, 130 + blockWidth / 2,150)) {
                let e = Engine.stack.get();
                Engine.last.text = e.value;
                Engine.last.color = e.color;
                UI.redraw();
            }
        }
    }

    static getMousePos(evt) {
        var rect = this.canvas.getBoundingClientRect();
        return {
            x : evt.clientX - rect.left,
            y : evt.clientY - rect.top
        };
    }
    static redraw() {
        this.clear();
        this.drawable.forEach(d => d.draw());
    }

    static clear() {
        this.ctx.fillStyle = "#fff";
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    static drawRectWithText(x, y, w, h, opts) {
        this.ctx.textAlign = "center";
        this.ctx.font = "bold 20px serif";
        this.ctx.fillStyle = opts.color;
        this.ctx.fillRect(x - w / 2, y - h, w, h);
        this.ctx.fillStyle = "#000";
        this.ctx.strokeRect(x - w / 2, y - h, w, h);
        this.ctx.fillText(opts.value, x, y - 10);
    }

    static drawPointer(x, y, color = "#03fd03") {
        this.ctx.strokeStyle = color;
        this.ctx.beginPath();
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x + 5, y - 5);
        this.ctx.moveTo(x, y);
        this.ctx.lineTo(x + 5, y + 5);
        this.ctx.stroke();
        this.ctx.strokeStyle = "#ff0000";
    }
}
    class Engine {
        static init() {
            this.last = new Rect(130 , 145, 'pop');
            this.next = new Rect(130, 105, Engine.irandom(1));
            this.stack = new Stack();
            this.stack.add({value: 0, color: randomColor()});
            UI.redraw();
        }
        static irandom(n) {
            return Math.floor(Math.round(Math.random()*n));
        } 
    }

    class Rect extends IDrawable {
        constructor(x, y, text) {
            super();
            this.color = randomColor();
            this.text = text;
            this.x = x;
            this.y = y;
            this.w = UI.canvas.width / 5 - 10;
            this.h = 40;
        }

        draw() {
            UI.drawRectWithText(this.x, this.y, this.w, this.h, {value: this.text, color: this.color});
        }
    }
    UI.init();
    Engine.init();



</script>
```

![60af92cb51fb1397dcdc63109adb07c2.png](../../../_resources/60af92cb51fb1397dcdc63109adb07c2.png)




# задание 007


  
  

```html
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title></title>
</head>

<body>

<!-- Для объектно-ориентированной программной модели циклической очереди разработать пользовательский графический интерфейс. -->

    <canvas width="1200px" height="500px"></canvas>
    <script type="text/javascript">
        const colors = ["#0076c0"];
        var randomColor = () => { return colors[Engine.irandom(colors.length - 1)] };
        class IDrawable {
            constructor() {
                UI.drawable.push(this);
            }
            draw() {}
        }
        class CycledQueue extends IDrawable {
            constructor(size) {
                super();
                this.size = size;
                this.elems = [];
                for (let i = 0; i < size; i++) this.elems.push({ value: '_', color: '#00cc00'});
                this.begin = -1;
                this.end = -1;
            }
            add(e) {
                if (this.isFull()) {
                    alert('overflow');
                } else {
                    if (this.begin == -1) this.begin = 0;
                    this.end = (this.end + 1) % this.size;
                    this.elems[this.end] = e;
                }
            }
            get() {
                if (this.isEmpty()) {
                    alert('empty');
                } else {
                    let e = this.elems[this.begin];
                    this.elems[this.begin] = { value: '_', color: '#00cc00'};
                    if (this.begin == this.end) {
                        this.begin = this.end = -1;
                    } else {
                        this.begin = (this.begin + 1) % this.size;
                    }
                    return e;
                }
            }
            isFull() {
                return (this.begin == this.end + 1) || (this.begin == 0 && this.end == this.size - 1);
            }
            isEmpty() {
                return this.begin == -1;
            }
            draw() {
                for (let i = 0; i < this.elems.length; i++) {
                    UI.drawRectWithText(50 + 50 * (this.elems.length1 + i), 50, 50, 50, this.elems[i]);
                }

            }
        }
        class UI {
            static init() {
                this.canvas = document.getElementsByTagName('canvas')[0];
                this.ctx = this.canvas.getContext('2d');
                this.drawable = [];
                this.canvas.onclick = (ev) => {
                    let mouse = this.getMousePos(ev);
                    let blockWidth = UI.canvas.width / 3 - 10;
                    let blockHeight = 50;

                    function inBounds(minX, minY, maxX, maxY) {
                        return (mouse.x > minX && mouse.x < maxX) && (mouse.y > minY && mouse.y < maxY);
                    }
                    if (inBounds(10, 100 - blockHeight, 10 + blockWidth, 100)) {
                        Engine.queue.add({ value: Engine.next.text, color: Engine.next.color });
                        Engine.next.text = Engine.irandom(9);
                        Engine.next.color = randomColor();
                        UI.redraw();
                    }
                    if (inBounds(10, 150 - blockHeight, 10 + blockWidth, 150)) {
                        let e = Engine.queue.get()
                        Engine.last.text = e.value;
                        Engine.last.color = e.color;
                        UI.redraw();
                    }
                }
            }
            static getMousePos(evt) {
                var rect = this.canvas.getBoundingClientRect();
                return {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
            }
            static redraw() {
                this.clear();
                this.drawable.forEach(d => d.draw());
            }
            static clear() {
                this.ctx.fillStyle = "#59c32b";
                this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
            }
            static drawRectWithText(x, y, w, h, opts) {
                this.ctx.textAlign = "center";
                this.ctx.font = "bold 20px serif";
                this.ctx.fillStyle = opts.color;
                this.ctx.fillRect(x - w / 2, y - h, w, h);
                this.ctx.fillStyle = "#000082";
                this.ctx.strokeRect(x - w / 2, y - h, w, h);
                this.ctx.fillText(opts.value, x, y - 10);
            }
            static drawPointer(x, y, color = "#03fd03") {
                this.ctx.strokeStyle = color;
                this.ctx.beginPath();
                this.ctx.moveTo(x, y);
                this.ctx.lineTo(x + 5, y - 5);
                this.ctx.moveTo(x, y);
                this.ctx.lineTo(x + 5, y + 5);
                this.ctx.stroke();
                this.ctx.strokeStyle = "#ff0000";
            }
        }
        class Engine {
            static init() {
                this.last = new Rect(150, 145, 'dequeue');
                this.next = new Rect(150, 100, Engine.irandom(1));
                this.queue = new CycledQueue(8);
                UI.redraw();
            }
            static irandom(n) {
                return Math.floor(Math.round(Math.random() * n));
            }
        }
        class Rect extends IDrawable {
            constructor(x, y, text) {
                super();
                this.color = randomColor();
                this.text = text;
                this.x = x;
                this.y = y;
                this.w = UI.canvas.width / 3 - 10;
                this.h = 50;
            }
            draw() {
                UI.drawRectWithText(this.x, this.y, this.w, this.h, { value: this.text, color: this.color });
            }
        }
        UI.init();
        Engine.init();
    </script>





    <style>
        canvas {
            display: block;
            margin: 0 auto;
        }
    </style>
</body>

</html>
```

![e09015185994ea4baa58c7d2c0469ba6.png](../../../_resources/e09015185994ea4baa58c7d2c0469ba6.png)

# задание 008

```html
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title></title>
</head>

<body>

    <!-- Для объектно-ориентированной программной модели двусвязного списка разработать пользовательский графический интерфейс.-->





    <canvas width="500xp" height="600px"></canvas>
    <script type="text/javascript">

        const colors = ["#0e6af4"];
        var randomColor = () => { return colors[Engine.irandom(colors.length - 1)] };
        class IDrawable {
            constructor() {
                UI.drawable.push(this);
            }
            draw() { }
        }
        class Node {
            constructor(val, prev, next) {
                this.value = val;
                this.prev = prev;
                this.next = next;
            }
        }
        class List extends IDrawable {
            constructor() {
                super();
                this.begin = null;
                this.end = null;
                this.size = 0;
            }
            add(e) {
                if (!this.begin) {
                    this.end = this.begin = new Node(e, null, null);
                    this.size++;
                }
                else {
                    this.size++;
                    this.end.next = new Node(e, this.end, null);
                    this.end = this.end.next;
                    return this.end;
                }
            }
            remove(e) {
                try {
                    if (e.prev) e.prev.next = e.next;
                    if (e.next) e.next.prev = e.prev;
                    if (this.begin == e) this.begin = e.next;
                    if (this.end == e) this.end = e.prev;
                    this.size--;
                }
                catch (err) {
                    console.log(err);
                }
            }
            removeFromPos(n) {
                if (n >= this.size) {
                    alert("Out of bounds.");
                } else {
                    let i = 0;
                    let c = this.begin;
                    let e = true;

                    while (i < n && e) {
                        if (c.next) { c = c.next; i++ }
                        else e = false;
                    }
                    if (!e) {
                        alert("Unreachable.")
                    } else {
                        this.remove(c);
                        return c.value;
                    }
                }
            }
            toArray() {
                let r = [];
                let c = this.begin;
                while (c.next) {
                    r.push(c);
                    c = c.next;
                }
                return r;
            }

            draw() {
                let c = this.begin;
                let dy = 0;
                let d = true;
                while (d) {
                    UI.drawRectWithText(UI.canvas.width / 2, UI.canvas.height - 50 * dy++, UI.canvas.width / 3 - 10, 50, c.value);
                    if (c.prev) UI.drawPointer(UI.canvas.width / 2 + 10, UI.canvas.height - 50 * dy + 10, true);
                    if (c.next) UI.drawPointer(UI.canvas.width / 2 - 10, UI.canvas.height - 50 * dy + 5);
                    if (c.next) c = c.next;
                    else d = false;
                }
            }
        }
        class UI {
            static init() {
                this.canvas = document.getElementsByTagName('canvas')[0];
                this.ctx = this.canvas.getContext('2d');
                this.drawable = [];
                this.canvas.onclick = (ev) => {
                    let mouse = this.getMousePos(ev);
                    let blockWidth = UI.canvas.width / 2 - 100;
                    let blockHeight = 50;

                    function inBounds(minX, minY, maxX, maxY) {
                        return (mouse.x > minX && mouse.x < maxX) && (mouse.y > minY && mouse.y < maxY);
                    }
                    if (inBounds(UI.canvas.width * 5 / 6 - blockWidth / 2, UI.canvas.height - blockHeight, UI.canvas.width * 5 / 6 + blockWidth / 2, UI.canvas.height)) {
                        Engine.list.add({ value: Engine.next.text, color: Engine.next.color });
                        Engine.next.text = Engine.irandom(3);
                        Engine.next.color = randomColor();
                        UI.redraw();
                    }
                    if (inBounds(UI.canvas.width * 1 / 2 - blockWidth / 2, UI.canvas.height - blockHeight * Engine.list.size, UI.canvas.width * 1 / 2 + blockWidth / 2, UI.canvas.height)) {
                        let i = Engine.list.size - 1 - Math.floor((mouse.y - (UI.canvas.height - blockHeight * Engine.list.size)) / blockHeight);
                        let e = Engine.list.removeFromPos(i)
                        Engine.last.text = e.value;
                        Engine.last.color = e.color;
                        UI.redraw();
                    }
                }
            }
            static getMousePos(evt) {
                var rect = this.canvas.getBoundingClientRect();
                return {
                    x: evt.clientX - rect.left,
                    y: evt.clientY - rect.top
                };
            }
            static redraw() {
                this.clear();
                this.drawable.forEach(d => d.draw());
            }
            static clear() {
                this.ctx.fillStyle = "#f78cf5";
                this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
            }
            static drawRectWithText(x, y, w, h, opts) {
                this.ctx.textAlign = "center";
                this.ctx.font = "bold 20px serif";
                this.ctx.fillStyle = opts.color;
                this.ctx.fillRect(x - w / 2, y - h, w, h);
                this.ctx.fillStyle = "#000";
                this.ctx.strokeRect(x - w / 2, y - h, w, h);
                this.ctx.fillText(opts.value, x, y - 10);
            }
            static drawPointer(x, y, r = false, color = "#000") {
                let k = 2 * r - 1;
                this.ctx.strokeStyle = color;
                this.ctx.beginPath();
                this.ctx.moveTo(x, y);
                this.ctx.lineTo(x - 5, y - 5 * k);
                this.ctx.moveTo(x, y);
                this.ctx.lineTo(x + 5, y - 5 * k);
                this.ctx.stroke();
                this.ctx.strokeStyle = "#000";
            }
        }
        class Engine {
            static init() {
                this.last = new Rect(UI.canvas.width / 6, UI.canvas.height, '?');
                this.next = new Rect(UI.canvas.width / 2 + UI.canvas.width / 3, UI.canvas.height, Engine.irandom(3));
                this.list = new List();
                this.list.add({ value: 0, color: randomColor() });
                UI.redraw();
            }
            static irandom(n) {
                return Math.floor(Math.round(Math.random() * n));
            }
        }
        class Rect extends IDrawable {
            constructor(x, y, text) {
                super();
                this.color = randomColor();
                this.text = text;
                this.x = x;
                this.y = y;
                this.w = UI.canvas.width / 2 - 100;
                this.h = 50;
            }
            draw() {
                UI.drawRectWithText(this.x, this.y, this.w, this.h, { value: this.text, color: this.color });
            }
        }
        UI.init();
        Engine.init();
    </script>
</body>

</html>
```


![2f66777c1c99a684d96b894407af969c.png](../../../_resources/2f66777c1c99a684d96b894407af969c.png)