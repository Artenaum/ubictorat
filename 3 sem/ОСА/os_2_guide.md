


[TOC]


# backup 31.01.2021 18:25

```c++
#include <iostream>
#include <algorithm>
using namespace std;
FILE* fp;

struct seance
{
    int min;
    int hour;
    int day;
    int month;
} ;

struct cinema
{
    string cinema_name;
    seance time;
    int price;
    int viewers;
    bool isEmpty;
};

////////////////////////////////////////////////////////////////1. очистка данных переменной

void EmptyArray(int index, cinema* arr)
{
    arr[index].isEmpty = true; // делает массив пустым
}

////////////////////////////////////////////////////////////////2. поиск свободной переменной

int FindFirstEmpty(cinema* arr)
{
    int index = 0;
    while (!arr[index].isEmpty && index < 5) // пока элемент не пустой и пока индекс меньше размера то индекс увеличивается, иначе остановка
    {
        index++;
    }
    return index;
}

////////////////////////////////////////////////////////////////3. ввод структурной переменной

void InputElem(int index, cinema* arr) {
    seance tempTime;
    string tempName;
    int tempPrice, tempViewers;

    cout << "имя: "; // ввод данных в объект кино
    cin >> tempName;
    cout << "минута: ";
    cin >> tempTime.min;
    cout << "час: ";
    cin >> tempTime.hour;
    cout << "день: ";
    cin >> tempTime.day;
    cout << "месяц: ";
    cin >> tempTime.month;
    cout << "цена: ";
    cin >> tempPrice;
    cout << "зрителей: ";
    cin >> tempViewers;

    cout << '\n';

    arr[index].cinema_name = tempName;
    arr[index].time = tempTime;
    arr[index].price = tempPrice;
    arr[index].viewers = tempViewers;
    arr[index].isEmpty = false;
}

////////////////////////////////////////////////////////////////4. вывод выбранной переменной на экран

void OutputElem(int index, cinema* arr) {
    cout << "имя: " << arr[index].cinema_name << endl;
    cout << "дата: " << arr[index].time.hour << ':'
        << arr[index].time.min << ' '
        << arr[index].time.day << '.'
        << arr[index].time.month << endl;
    cout << "цена: " << arr[index].price << endl;
    cout << "зрителей: " << arr[index].viewers << endl;
    cout << '\n';
}

////////////////////////////////////////////////////////////////5. вывод на экран всех заполненных элементов

void OutputAllFull(cinema* arr) {
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            OutputElem(i, arr);
        }
    }
}

////////////////////////////////////////////////////////////////6. поиск в массиве данного значения или ближайшего к нему

void SearchExact(cinema* arr) {
    string s;
    cout << "ввод: ";
    cin >> s;

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            if (arr[i].cinema_name == s || arr[i].price == atoi(s.c_str()) || arr[i].viewers == atoi(s.c_str())) // если поиск равен чему-то в массиве кино,цены или зрителей то вывести
            {
                OutputElem(i, arr);
            }
            else cout << "нет результатов\n";
        }
    }
}

////////////////////////////////////////////////////////////////7. поиск индекса элемента с минимальным введенным значением

void SearchMin(cinema* arr, int fieldNum) {
    int min;

    if (fieldNum == 1)
    {
        min = arr[0].price;
    }
    else if (fieldNum == 2)
    {
        min = arr[0].viewers;
    }

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            if (fieldNum == 1)
            {
                if (arr[i].price < min)
                {
                    min = arr[i].price;
                }
            }
            else if (fieldNum == 2)
            {
                if (arr[i].viewers < min)
                {
                    min = arr[i].viewers;
                }
            }
        }
    }

    for (int i = 0; i < 5; i++)
    {
        if (fieldNum == 1)
        {
            if (arr[i].price == min)
            {
                OutputElem(i, arr);
            }
        }
        else if (fieldNum == 2)
        {
            if (arr[i].viewers == min)
            {
                OutputElem(i, arr);
            }
        }
    }
}

////////////////////////////////////////////////////////////////

bool compNameMinToMax(cinema a, cinema b)
{
    return a.cinema_name < b.cinema_name;
}

////////////////////////////////////////////////////////////////


bool compPriceMinToMax(cinema a, cinema b)
{
    return a.price < b.price;
}

////////////////////////////////////////////////////////////////


bool compViewersMinToMax(cinema a, cinema b) {
    return a.viewers < b.viewers;
}

////////////////////////////////////////////////////////////////
bool compNameMaxToMin(cinema a, cinema b) {
    return a.cinema_name > b.cinema_name;
}

////////////////////////////////////////////////////////////////
bool compPriceMaxToMin(cinema a, cinema b) {
    return a.price > b.price;
}

////////////////////////////////////////////////////////////////
bool compViewersMaxToMin(cinema a, cinema b) {
    return a.viewers > b.viewers;
}

////////////////////////////////////////////////////////////////8. сортировка массива в порядке возрастания

void SortMinToMax(cinema* arr, int fieldNum) {
    switch (fieldNum)
    {
    case 1:
    {
        sort(arr, arr + 5, compNameMinToMax);
        OutputAllFull(arr);
        break;
    }
    case 2:
    {
        sort(arr, arr + 5, compPriceMinToMax);
        OutputAllFull(arr);
        break;
    }
    case 3:
    {
        sort(arr, arr + 5, compViewersMinToMax);
        OutputAllFull(arr);
        break;
    }
    default:
    {
        cout << "неверный ввод";
        break;
    }
    }
}

////////////////////////////////////////////////////////////////9. сортировка массива в порядке убывания

void SortMaxToMin(cinema* arr, int fieldNum) {
    switch (fieldNum)
    {
    case 1:
    {
        sort(arr, arr + 5, compNameMaxToMin);
        OutputAllFull(arr);
        break;
    }
    case 2:
    {
        sort(arr, arr + 5, compPriceMaxToMin);
        OutputAllFull(arr);
        break;
    }
    case 3:
    {
        sort(arr, arr + 5, compViewersMaxToMin);
        OutputAllFull(arr);
        break;
    }
    default:
    {
        cout << "неверный ввод";
        break;
    }
    }
}

////////////////////////////////////////////////////////////////10. запись данных в файл

void ReadFile() {
    cinema arr[5];
    fopen_s(&fp, "seances.txt", "r");
    for (int i = 0; i < 5; i++)
    {
        fread(&arr[i], sizeof(cinema), 1, fp); // метод читает файл 
    }
    fclose(fp);
    OutputAllFull(arr);
}

////////////////////////////////////////////////////////////////11. чтение файла

void WriteFile(cinema* arr) {
    fopen_s(&fp, "seances.txt", "w");
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty) // если файл не пустой то идет запись в файл
        {
            fwrite(&arr[i], sizeof(cinema), 1, fp);
        }
    }
    fclose(fp);
}

////////////////////////////////////////////////////////////////

int main() {
    setlocale(LC_ALL, "Russian");
    FILE* f;
    cinema m[5];
    int selection;
    int index;
    bool exit = false;

    do
    {
        cout << '\n';
        cout << "1: название кино" << endl;
        cout << "2: показать кино" << endl;
        cout << "3: показать все фильмы" << endl;
        cout << "4: показать свободный элемент" << endl;
        cout << "5: отметить свободным" << endl;
        cout << "6: точный поиск" << endl;
        cout << "7: найти минимальный элемент" << endl;
        cout << "8: сортировать в порядке возрастания" << endl;
        cout << "9: сортировать в порядке убывания" << endl;
        cout << "10: сохранить" << endl;
        cout << "11: открыть" << endl;
        cout << "12: выход" << endl;

        cin >> selection;
        cout << '\n';

        switch (selection)
        {
        case 1:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            InputElem(index, m);
            break;
        }
        case 2:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            OutputElem(index, m);
            break;
        }
        case 3:
        {
            OutputAllFull(m);
            break;
        }
        case 4:
        {
            FindFirstEmpty(m);
            break;
        }
        case 5:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            EmptyArray(index, m);
            break;
        }
        case 6:
        {
            SearchExact(m);
            break;
        }
        case 7:
        {
            int field;
            cout << "Цена, зрители: ";
            cin >> field;
            SearchMin(m, field);
            break;
        }
        case 8:
        {
            int field;
            cout << "Имя, цена, зрители: ";
            cin >> field;
            SortMinToMax(m, field);
            break;
        }
        case 9:
        {
            int field;
            cout << "Имя, цена, зрители: ";
            cin >> field;
            SortMaxToMin(m, field);
            break;
        }
        case 10:
        {
            WriteFile(m);
            break;
        }
        case 11:
        {
            ReadFile();
            break;
        }
        case 12:
        {
            exit = true;
            break;
        }
        default:
        {
            cout << "ошибка открытия файла" << endl;
            break;
        }
        }
    } while (!exit);
    return 0;
}


```


# backup 31.01.2021 14:19 (основной)

```c++
#include <iostream>
#include <algorithm>
using namespace std;
FILE* fp;

struct seance
{
    int min;
    int hour;
    int day;
    int month;
} ;

struct cinema
{
    string cinema_name;
    seance time;
    int price;
    int viewers;
    bool isEmpty;
};

////////////////////////////////////////////////////////////////

void EmptyArray(int index, cinema* arr)
{
    arr[index].isEmpty = true; // делает массив пустым
}

////////////////////////////////////////////////////////////////

int FindFirstEmpty(cinema* arr)
{
    int index = 0;
    while (!arr[index].isEmpty && index < 5) // пока элемент не пустой и пока индекс меньше размера то индекс увеличивается, иначе остановка
    {
        index++;
    }
    return index;
}

////////////////////////////////////////////////////////////////

void InputElem(int index, cinema* arr) {
    seance tempTime;
    string tempName;
    int tempPrice, tempViewers;

    cout << "имя: "; // ввод данных в объект кино
    cin >> tempName;
    cout << "минута: ";
    cin >> tempTime.min;
    cout << "час: ";
    cin >> tempTime.hour;
    cout << "день: ";
    cin >> tempTime.day;
    cout << "месяц: ";
    cin >> tempTime.month;
    cout << "цена: ";
    cin >> tempPrice;
    cout << "зрителей: ";
    cin >> tempViewers;

    cout << '\n';

    arr[index].cinema_name = tempName;
    arr[index].time = tempTime;
    arr[index].price = tempPrice;
    arr[index].viewers = tempViewers;
    arr[index].isEmpty = false;
}

////////////////////////////////////////////////////////////////

void OutputElem(int index, cinema* arr) {
    cout << "имя: " << arr[index].cinema_name << endl;
    cout << "дата: " << arr[index].time.hour << ':'
        << arr[index].time.min << ' '
        << arr[index].time.day << '.'
        << arr[index].time.month << endl;
    cout << "цена: " << arr[index].price << endl;
    cout << "зрителей: " << arr[index].viewers << endl;
    cout << '\n';
}

////////////////////////////////////////////////////////////////

void OutputAllFull(cinema* arr) {
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            OutputElem(i, arr);
        }
    }
}

////////////////////////////////////////////////////////////////

void SearchExact(cinema* arr) {
    string s;
    cout << "ввод: ";
    cin >> s;

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            if (arr[i].cinema_name == s || arr[i].price == atoi(s.c_str()) || arr[i].viewers == atoi(s.c_str()))
            {
                OutputElem(i, arr);
            }
            else cout << "нет результатов\n";
        }
    }
}

////////////////////////////////////////////////////////////////

void SearchMin(cinema* arr, int fieldNum) {
    int min;

    if (fieldNum == 1)
    {
        min = arr[0].price;
    }
    else if (fieldNum == 2)
    {
        min = arr[0].viewers;
    }

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty)
        {
            if (fieldNum == 1)
            {
                if (arr[i].price < min)
                {
                    min = arr[i].price;
                }
            }
            else if (fieldNum == 2)
            {
                if (arr[i].viewers < min)
                {
                    min = arr[i].viewers;
                }
            }
        }
    }

    for (int i = 0; i < 5; i++)
    {
        if (fieldNum == 1)
        {
            if (arr[i].price == min)
            {
                OutputElem(i, arr);
            }
        }
        else if (fieldNum == 2)
        {
            if (arr[i].viewers == min)
            {
                OutputElem(i, arr);
            }
        }
    }
}

////////////////////////////////////////////////////////////////

bool compNameMinToMax(cinema a, cinema b)
{
    return a.cinema_name < b.cinema_name;
}

////////////////////////////////////////////////////////////////


bool compPriceMinToMax(cinema a, cinema b)
{
    return a.price < b.price;
}

////////////////////////////////////////////////////////////////


bool compViewersMinToMax(cinema a, cinema b) {
    return a.viewers < b.viewers;
}

////////////////////////////////////////////////////////////////
bool compNameMaxToMin(cinema a, cinema b) {
    return a.cinema_name > b.cinema_name;
}

////////////////////////////////////////////////////////////////
bool compPriceMaxToMin(cinema a, cinema b) {
    return a.price > b.price;
}

////////////////////////////////////////////////////////////////
bool compViewersMaxToMin(cinema a, cinema b) {
    return a.viewers > b.viewers;
}

////////////////////////////////////////////////////////////////

void SortMinToMax(cinema* arr, int fieldNum) {
    switch (fieldNum)
    {
    case 1:
    {
        sort(arr, arr + 5, compNameMinToMax);
        OutputAllFull(arr);
        break;
    }
    case 2:
    {
        sort(arr, arr + 5, compPriceMinToMax);
        OutputAllFull(arr);
        break;
    }
    case 3:
    {
        sort(arr, arr + 5, compViewersMinToMax);
        OutputAllFull(arr);
        break;
    }
    default:
    {
        cout << "неверный ввод";
        break;
    }
    }
}

////////////////////////////////////////////////////////////////

void SortMaxToMin(cinema* arr, int fieldNum) {
    switch (fieldNum)
    {
    case 1:
    {
        sort(arr, arr + 5, compNameMaxToMin);
        OutputAllFull(arr);
        break;
    }
    case 2:
    {
        sort(arr, arr + 5, compPriceMaxToMin);
        OutputAllFull(arr);
        break;
    }
    case 3:
    {
        sort(arr, arr + 5, compViewersMaxToMin);
        OutputAllFull(arr);
        break;
    }
    default:
    {
        cout << "неверный ввод";
        break;
    }
    }
}

////////////////////////////////////////////////////////////////

void ReadFile() {
    cinema arr[5];
    fopen_s(&fp, "seances.txt", "r");
    for (int i = 0; i < 5; i++)
    {
        fread(&arr[i], sizeof(cinema), 1, fp); // метод читает файл 
    }
    fclose(fp);
    OutputAllFull(arr);
}

void WriteFile(cinema* arr) {
    fopen_s(&fp, "seances.txt", "w");
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].isEmpty) // если файл не пустой то идет запись в файл
        {
            fwrite(&arr[i], sizeof(cinema), 1, fp);
        }
    }
    fclose(fp);
}

////////////////////////////////////////////////////////////////

int main() {
    setlocale(LC_ALL, "Russian");
    FILE* f;
    cinema m[5];
    int selection;
    int index;
    bool exit = false;

    do
    {
        cout << '\n';
        cout << "1: название кино" << endl;
        cout << "2: показать кино" << endl;
        cout << "3: показать все фильмы" << endl;
        cout << "4: показать свободный элемент" << endl;
        cout << "5: отметить свободным" << endl;
        cout << "6: точный поиск" << endl;
        cout << "7: найти минимальный элемент" << endl;
        cout << "8: сортировать в порядке возрастания" << endl;
        cout << "9: сортировать в порядке убывания" << endl;
        cout << "10: сохранить" << endl;
        cout << "11: открыть" << endl;
        cout << "12: выход" << endl;

        cin >> selection;
        cout << '\n';

        switch (selection)
        {
        case 1:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            InputElem(index, m);
            break;
        }
        case 2:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            OutputElem(index, m);
            break;
        }
        case 3:
        {
            OutputAllFull(m);
            break;
        }
        case 4:
        {
            FindFirstEmpty(m);
            break;
        }
        case 5:
        {
            cout << "введите индекс" << endl;
            cin >> index;
            EmptyArray(index, m);
            break;
        }
        case 6:
        {
            SearchExact(m);
            break;
        }
        case 7:
        {
            int field;
            cout << "Цена, зрители: ";
            cin >> field;
            SearchMin(m, field);
            break;
        }
        case 8:
        {
            int field;
            cout << "Имя, цена, зрители: ";
            cin >> field;
            SortMinToMax(m, field);
            break;
        }
        case 9:
        {
            int field;
            cout << "Имя, цена, зрители: ";
            cin >> field;
            SortMaxToMin(m, field);
            break;
        }
        case 10:
        {
            WriteFile(m);
            break;
        }
        case 11:
        {
            ReadFile();
            break;
        }
        case 12:
        {
            exit = true;
            break;
        }
        default:
        {
            cout << "ошибка открытия файла" << endl;
            break;
        }
        }
    } while (!exit);
    return 0;
}


```



# backup 29.01.2021 10:44

```c++
#define  _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <iomanip>
#include <stdio.h> 
#include <stdlib.h>
#include <fstream>
#include <string>
#include <Windows.h>
#include <cstdio>



const int MAX = 100;
// Количество элементов массива структур
const int arr = 3; 
using namespace std;

struct  student {  // Структура студент
    int name = 0;
    int familia = 0;
    int patronymic = 0;
};

typedef struct klient { // Структура с перечнем полей
    char name[MAX]; // Поле ФИО
    student birthname; // Поле даты рождения
    char address[MAX]; // Поле Адреса
    bool inform = false; // Переменная, отвечающая за "заполненость" переменной, "false" - незаполнена, "true" - заполнена
} klient;


// 1. очистка

void clean(klient* mas, int i) {
    mas[i].name[MAX] = { 0 };
    mas[i].birthname.name = 0;
    mas[i].birthname.familia = 0;
    mas[i].birthname.patronymic = 0;
    mas[i].address[MAX] = { 0 };
    mas[i].inform = false; // Все данные элемента обнуляются, булева переменная inform принимает значение false
    cout << endl << "Выбранный " << i << " элемент был очищен" << endl;
}

// 2. поиск свободной переменной

int searchclean(klient* mas) {
    int element = 0;
    while (mas[element].inform == true && element < arr) { // Проходимся по всем элементам, пока булевая переменная inform не окажется со значением "false", то есть структурная переменная пустая
        element++;
    }
    if (element < arr) {
        return element;
    }
    if (element >= arr) { // Если индекс равен размеру или больше размера массива, то свободных элементов нет
        cout << endl << "Свободных элементов нет, будет возвращено -1" << endl;
        return -1;
    }
}


// 3.Ввод элементов с клавиатуры

void put(klient* mas, int i) { 
    if ((i < 0) || (i >= arr)) { // Проверка существования элемента
        cout << "Элемента №" << i << " не существует" << endl;
        exit(3);
    }
    char ch;
    cout << endl << "Введите Фамилию И.О.: ";
    cin.get();
    cin.getline(mas[i].name, MAX);
    cout << "Введите дату рождения: ";
    cin >> mas[i].birthname.name >> ch >> mas[i].birthname.familia >> ch >> mas[i].birthname.patronymic;
    cout << "Введите адрес: ";
    cin.get();
    cin.getline(mas[i].address, MAX);
    mas[i].inform = true;
    cout << endl;
}


// 4.Вывод элементов структуры на экран

void display(klient* mas, int i) { 
    if ((i < 0) || (i >= arr)) {// Проверка существования элемента
        cout << "Элемента №" << i << " не существует" << endl;
        exit(3);
    }
    if (mas[i].inform == true) {// Проверка наполнения элемента
        cout << endl << "Выбран " << i << " элемент" << endl << "Информация: " << endl << mas[i].name << " " << mas[i].birthname.name << "." << mas[i].birthname.familia << "." << mas[i].birthname.patronymic << " " << mas[i].address << endl;
    }
    else {
        cout << "Выбранный " << i << " элемент " << "пуст" << endl;
    }
}

// 5. Вывод всех элементов на экран

void displayall(klient* mas) { 
    int element = 0;
    cout << endl << "Имеющиеся данные: " << endl;
    while (element < arr) {
        if (mas[element].inform == true) {
            if ((mas[element].birthname.name < 10) && (mas[element].birthname.familia < 10)) { // Необходимые условия для вывода даты в формате dd.mm.yyyy
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << "  0" << mas[element].birthname.name << ".0" << mas[element].birthname.familia << "." << mas[element].birthname.patronymic << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthname.name < 10) && (mas[element].birthname.familia >= 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << "  0" << mas[element].birthname.name << "." << mas[element].birthname.familia << "." << mas[element].birthname.patronymic << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthname.name >= 10) && (mas[element].birthname.familia < 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << " " << mas[element].birthname.name << ".0" << mas[element].birthname.familia << "." << mas[element].birthname.patronymic << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthname.name >= 10) && (mas[element].birthname.familia >= 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << " " << mas[element].birthname.name << "." << mas[element].birthname.familia << "." << mas[element].birthname.patronymic << "  " << setw(20) << mas[element].address << endl;
            }

        }
        element++;
    }
};

// 6. Поиск структур с заданным значением поля

void search(klient* mas) { 
    int vyb, i, j, number = 0, count, maxarr = -1, min, now;
    char search[MAX];
    student search2;
    char ch;
    cout << endl << "Выберите строку, по которой хотите выполнить поиск: " << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb)
    {
    case 1:
        cout << "Введите ФИО: ";
        cin.get();
        cin.getline(search, MAX);
        for (i = 0; i < arr; i++) {
            count = 0;
            for (j = 0; j < strlen(mas[i].name); j++) {
                if (mas[i].name[j] == search[j]) { // Если символ ФИО элемента совпадает с символом введённого слова, то счётчик увеличивается
                    count++;
                }
                if (count > maxarr) {
                    maxarr = count;
                    number = i;
                }
            }
        }
        cout << "Элемент, который вы искали: " << number << endl;
        display(mas, number);
        break;
    case 2:
        min = 99999999;
        cout << "Введите дату рождения: ";
        cin >> search2.name >> ch >> search2.familia >> ch >> search2.patronymic;
        for (i = 0; i < arr; i++) {
            now = abs(mas[i].birthname.name + (mas[i].birthname.familia - 1) * 30 + mas[i].birthname.patronymic * 12 * 30 - (search2.name + (search2.familia - 1) * 30 + search2.patronymic * 12 * 30));
            if (now < min) {
                min = now;
                number = i;
            }

        }
        cout << "Элемент, который вы искали: ";
        display(mas, number);
        break;
    case 3:
        cout << "Введите Адрес: ";
        cin.get();
        cin.getline(search, MAX);
        for (i = 0; i < arr; i++) {
            count = 0;
            for (j = 0; j < strlen(mas[i].address); j++) {
                if (mas[i].address[j] == search[j]) { // Если символ Адреса элемента совпадает с символом введённого слова, то счётчик увеличивается
                    count++;
                }
                if (count > maxarr) {
                    maxarr = count;
                    number = i;
                }
            }
        }
        cout << "Элемент, который вы искали: " << number << endl;
        display(mas, number);
        break;
    }
}

// 7.Поиск минимального значения 

void minimal(klient* mas) { 
    int vyb, i, j, imin = 0, now;
    student min;
    cout << endl << "Выберите строку, в которой хотите найти минимальное значение " << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        while (mas[imin].inform == false) { // Ищем первый заполненный элемент и делаем его минимальным
            imin++;
        }
        i = imin + 1;
        for (i; i < arr; i++) {
            if (mas[i].inform != false) {
                for (j = 0; j < strlen(mas[i].name); j++) {
                    if (mas[i].name[j] < mas[imin].name[j]) { // Если символ текущего элемента меньше символа минимально элемента, то текущий элемент становится минимальным
                        imin = i;
                        break;
                    }
                    if (mas[i].name[j] > mas[imin].name[j]) {// Если символ текущего элемента больше символа минимально элемента, то цикл прерывается, и происходит переход к следующему символу
                        break;
                    }
                }
            }
        }
        cout << "Элемент с минимальным значением ФИО: " << endl;
        display(mas, imin);
        break;
    case 2:
        i = 0;
        while (mas[i].inform == false) {
            i++;
        }
        min = mas[i].birthname;
        imin = i;
        i = i + 1;
        for (i; i < arr; i++) {
            if (mas[i].inform != false) {
                if (mas[i].birthname.patronymic < min.patronymic) {
                    min = mas[i].birthname;
                    imin = i;
                }
                else if (mas[i].birthname.patronymic == min.patronymic) {
                    if (mas[i].birthname.familia < min.familia) {
                        min = mas[i].birthname;
                        imin = i;
                    }
                    else if (mas[i].birthname.familia == min.familia) {
                        if (mas[i].birthname.name < min.name) {
                            min = mas[i].birthname;
                            imin = i;
                        }
                    }
                }
            }
        }
        cout << "Элемент с минимальным значением даты рождения: " << endl;
        display(mas, imin);
        break;
    case 3:
        while (mas[imin].inform == false) {
            imin++;
        }
        i = imin + 1;
        for (i; i < arr; i++) {
            for (j = 0; j < strlen(mas[i].address); j++) {
                if (mas[i].address[j] < mas[imin].address[j]) {
                    imin = i;
                    break;
                }
                if (mas[i].address[j] > mas[imin].address[j]) {
                    break;
                }
            }
        }
        cout << "Элемент с минимальным значением адреса: " << endl;
        display(mas, imin);
        break;
    }
}

// 8.Сортировка в порядке возрастания заданного поля

void sortvozr(klient* mas) { 
    int vyb, i, j;
    cout << endl << "Выберите строку, по которой хотите произвести сортировку в порядке возрастания" << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < arr - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].name) && k < strlen(mas[i + 1].name); k++) {
                    if (mas[i].name[k] < mas[i + 1].name[k]) {
                        break;
                    }
                    if (mas[i].name[k] > mas[i + 1].name[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;
    case 2:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (i = 0; i < arr - 1; i++) {
                if (mas[i].birthname.patronymic > mas[i + 1].birthname.patronymic) {
                    klient x = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = x;
                }
                else if (mas[i].birthname.patronymic == mas[i + 1].birthname.patronymic)
                {
                    if (mas[i].birthname.familia > mas[i + 1].birthname.familia) {
                        klient x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                    }
                    else if (mas[i].birthname.familia == mas[i + 1].birthname.familia)
                    {
                        if (mas[i].birthname.name > mas[i + 1].birthname.name) {
                            klient x = mas[i];
                            mas[i] = mas[i + 1];
                            mas[i + 1] = x;
                        }

                    }

                }
            }
        }
        break;
    case 3:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < arr - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].address) && k < strlen(mas[i + 1].address); k++) {
                    if (mas[i].address[k] < mas[i + 1].address[k]) {
                        break;
                    }
                    if (mas[i].address[k] > mas[i + 1].address[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;

    }
}

// 9.Сортировка в порядке убывания

void sortubiv(klient* mas) {  
    int vyb, i, j;
    cout << endl << "Выберите строку, по которой хотите произвести сортировку в порядке убывания" << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < arr - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].name) && k < strlen(mas[i + 1].name); k++) {
                    if (mas[i].name[k] > mas[i + 1].name[k]) {
                        break;
                    }
                    if (mas[i].name[k] < mas[i + 1].name[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;
    case 2:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (i = 0; i < arr - 1; i++) {
                if (mas[i].birthname.patronymic < mas[i + 1].birthname.patronymic) {
                    klient x = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = x;
                }
                else if (mas[i].birthname.patronymic == mas[i + 1].birthname.patronymic)
                {
                    if (mas[i].birthname.familia < mas[i + 1].birthname.familia) {
                        klient x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                    }
                    else if (mas[i].birthname.familia == mas[i + 1].birthname.familia)
                    {
                        if (mas[i].birthname.name < mas[i + 1].birthname.name) {
                            klient x = mas[i];
                            mas[i] = mas[i + 1];
                            mas[i + 1] = x;
                        }

                    }

                }
            }
        }
        break;
    case 3:
        for (j = 1; j < arr; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < arr - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].address) && k < strlen(mas[i + 1].address); k++) {
                    if (mas[i].address[k] > mas[i + 1].address[k]) {
                        break;
                    }
                    if (mas[i].address[k] < mas[i + 1].address[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;

    }
}

// 10. чтение файла

void read(klient* mas) {
    FILE* f;
    if ((f = fopen("myFile.txt", "rb")) == NULL) exit(3);
    fread(mas, sizeof(mas[0]), arr, f);
    fclose(f);
}

// 11. запись

void write(klient* mas) {
    FILE* f;
    if ((f = fopen("myFile.txt", "rb")) == NULL) exit(3);
    fwrite(mas, sizeof(mas[0]), arr, f);
    fclose(f);
}



int main()
{
    setlocale(LC_ALL, "Russian");
    FILE* f;	// указатель связанный с файлом
    klient e;	// переменная
    klient mas[arr], mascopy[arr];	//массив
    int vyb, i;
    char ch;
    cout << "1 - Очистка структурной переменной" << endl 
         << "2 - Поиск свободной структурной переменной" << endl
         << "3 - Ввод элементов (полей) структуры с клавиатуры" << endl 
         << "4 - Вывод элементов (полей) структуры на монитор" << endl
         << "5 - Вывод на экран всех заполненных элементов " << endl 
         << "6 - Поиск в массиве структур элемента с заданным значением поля или с наиболее близким к нему по значению " << endl
         << "7 - Поиск в массиве структуры с минимальным значением заданного поля  " << endl 
         << "8 - Сортировка массива структур в порядке возрастания заданного поля " << endl
         << "9 - Сортировка массива структур в порядке убывания заданного поля " << endl 
         << "10 - Заполнить все элементы с клавиатуры" << endl;
    do {
        cout << "Выберите действие(цифру), которое хотите совершить: ";
        cin >> vyb;
        switch (vyb) {
        case 1:
            cout << "Введите элемент, который хотите очистить: ";
            cin >> i;
            clean(mas, i);
            break;
        case 2:
            cout << "Первая свободная структурная переменная - элемент №" << searchclean(mas) << endl;
            break;
        case 3:
            cout << "Введите № элемента, который хотите заполнить: ";
            cin >> i;
            put(mas, i);
            break;
        case 4:
            cout << "Введите № элемента, который хотите вывести на экран: ";
            cin >> i;
            display(mas, i);
            break;
        case 5:
            displayall(mas);
            break;
        case 6:
            search(mas);
            break;
        case 7:
            minimal(mas);
            break;
        case 8:
            sortvozr(mas);
            break;
        case 9:
            sortubiv(mas);
            break;
        case 10:
            for (i = 0; i < arr; i++) {
                put(mas, i);
            }
            break;
        }
        cout << endl << "Продолжить?(y/n) ";
        cin >> ch;
        cout << endl;
    } while (ch != 'n');
    write(mas);
    read(mascopy);
    displayall(mascopy);
}
```


# backup



```c++
#define  _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <iomanip>
#include <stdio.h> 
#include <stdlib.h>
#include <fstream>
#include <string>
#include <Windows.h>
#include <cstdio>



const int MAX = 100;
const int razm = 3; // Количество элементов массива структур
using namespace std;

struct  date {  // Структура даты
    int day = 0;
    int mounth = 0;
    int year = 0;
};

typedef struct klient { // Структура с перечнем полей
    char name[MAX]; // Поле ФИО
    date birthday; // Поле даты рождения
    char address[MAX]; // Поле Адреса
    bool inform = false; // Переменная, отвечающая за "заполненость" переменной, "false" - незаполнена, "true" - заполнена
} klient;


// 1. очистка

void clean(klient* mas, int i) { 
    mas[i].name[MAX] = { 0 };
    mas[i].birthday.day = 0;
    mas[i].birthday.mounth = 0;
    mas[i].birthday.year = 0;
    mas[i].address[MAX] = { 0 };
    mas[i].inform = false; // Все данные элемента обнуляются, булева переменная inform принимает значение false
    cout << endl << "Выбранный " << i << " элемент был очищен" << endl;
}

// 2. поиск свободной переменной

int searchclean(klient* mas) {
    int element = 0;
    while (mas[element].inform == true && element < razm) { // Проходимся по всем элементам, пока булевая переменная inform не окажется со значением "false", то есть структурная переменная пустая
        element++;
    }
    if (element < razm) {
        return element;
    }
    if (element >= razm) { // Если индекс равен размеру или больше размера массива, то свободных элементов нет
        cout << endl << "Свободных элементов нет, будет возвращено -1" << endl;
        return -1;
    }
}


// 3.Ввод элементов с клавиатуры

void put(klient* mas, int i) { 
    if ((i < 0) || (i >= razm)) { // Проверка существования элемента
        cout << "Элемента №" << i << " не существует" << endl;
        exit(3);
    }
    char ch;
    cout << endl << "Введите Фамилию И.О.: ";
    cin.get();
    cin.getline(mas[i].name, MAX);
    cout << "Введите дату рождения: ";
    cin >> mas[i].birthday.day >> ch >> mas[i].birthday.mounth >> ch >> mas[i].birthday.year;
    cout << "Введите адрес: ";
    cin.get();
    cin.getline(mas[i].address, MAX);
    mas[i].inform = true;
    cout << endl;
}


// 4.Вывод элементов структуры на экран

void display(klient* mas, int i) { 
    if ((i < 0) || (i >= razm)) {// Проверка существования элемента
        cout << "Элемента №" << i << " не существует" << endl;
        exit(3);
    }
    if (mas[i].inform == true) {// Проверка наполнения элемента
        cout << endl << "Выбран " << i << " элемент" << endl << "Информация: " << endl << mas[i].name << " " << mas[i].birthday.day << "." << mas[i].birthday.mounth << "." << mas[i].birthday.year << " " << mas[i].address << endl;
    }
    else {
        cout << "Выбранный " << i << " элемент " << "пуст" << endl;
    }
}

// 5. Вывод всех элементов на экран

void displayall(klient* mas) { 
    int element = 0;
    cout << endl << "Имеющиеся данные: " << endl;
    while (element < razm) {
        if (mas[element].inform == true) {
            if ((mas[element].birthday.day < 10) && (mas[element].birthday.mounth < 10)) { // Необходимые условия для вывода даты в формате dd.mm.yyyy
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << "  0" << mas[element].birthday.day << ".0" << mas[element].birthday.mounth << "." << mas[element].birthday.year << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthday.day < 10) && (mas[element].birthday.mounth >= 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << "  0" << mas[element].birthday.day << "." << mas[element].birthday.mounth << "." << mas[element].birthday.year << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthday.day >= 10) && (mas[element].birthday.mounth < 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << " " << mas[element].birthday.day << ".0" << mas[element].birthday.mounth << "." << mas[element].birthday.year << "  " << setw(20) << mas[element].address << endl;
            }
            if ((mas[element].birthday.day >= 10) && (mas[element].birthday.mounth >= 10)) {
                cout << setiosflags(ios::left) << setw(15) << mas[element].name << " " << mas[element].birthday.day << "." << mas[element].birthday.mounth << "." << mas[element].birthday.year << "  " << setw(20) << mas[element].address << endl;
            }

        }
        element++;
    }
};

// 6. Поиск структур с заданным значением поля

void search(klient* mas) { 
    int vyb, i, j, number = 0, count, maxrazm = -1, min, now;
    char search[MAX];
    date search2;
    char ch;
    cout << endl << "Выберите строку, по которой хотите выполнить поиск: " << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb)
    {
    case 1:
        cout << "Введите ФИО: ";
        cin.get();
        cin.getline(search, MAX);
        for (i = 0; i < razm; i++) {
            count = 0;
            for (j = 0; j < strlen(mas[i].name); j++) {
                if (mas[i].name[j] == search[j]) { // Если символ ФИО элемента совпадает с символом введённого слова, то счётчик увеличивается
                    count++;
                }
                if (count > maxrazm) {
                    maxrazm = count;
                    number = i;
                }
            }
        }
        cout << "Элемент, который вы искали: " << number << endl;
        display(mas, number);
        break;
    case 2:
        min = 99999999;
        cout << "Введите дату рождения: ";
        cin >> search2.day >> ch >> search2.mounth >> ch >> search2.year;
        for (i = 0; i < razm; i++) {
            now = abs(mas[i].birthday.day + (mas[i].birthday.mounth - 1) * 30 + mas[i].birthday.year * 12 * 30 - (search2.day + (search2.mounth - 1) * 30 + search2.year * 12 * 30));
            if (now < min) {
                min = now;
                number = i;
            }

        }
        cout << "Элемент, который вы искали: ";
        display(mas, number);
        break;
    case 3:
        cout << "Введите Адрес: ";
        cin.get();
        cin.getline(search, MAX);
        for (i = 0; i < razm; i++) {
            count = 0;
            for (j = 0; j < strlen(mas[i].address); j++) {
                if (mas[i].address[j] == search[j]) { // Если символ Адреса элемента совпадает с символом введённого слова, то счётчик увеличивается
                    count++;
                }
                if (count > maxrazm) {
                    maxrazm = count;
                    number = i;
                }
            }
        }
        cout << "Элемент, который вы искали: " << number << endl;
        display(mas, number);
        break;
    }
}

// 7.Поиск минимального значения 

void minimal(klient* mas) { 
    int vyb, i, j, imin = 0, now;
    date min;
    cout << endl << "Выберите строку, в которой хотите найти минимальное значение " << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        while (mas[imin].inform == false) { // Ищем первый заполненный элемент и делаем его минимальным
            imin++;
        }
        i = imin + 1;
        for (i; i < razm; i++) {
            if (mas[i].inform != false) {
                for (j = 0; j < strlen(mas[i].name); j++) {
                    if (mas[i].name[j] < mas[imin].name[j]) { // Если символ текущего элемента меньше символа минимально элемента, то текущий элемент становится минимальным
                        imin = i;
                        break;
                    }
                    if (mas[i].name[j] > mas[imin].name[j]) {// Если символ текущего элемента больше символа минимально элемента, то цикл прерывается, и происходит переход к следующему символу
                        break;
                    }
                }
            }
        }
        cout << "Элемент с минимальным значением ФИО: " << endl;
        display(mas, imin);
        break;
    case 2:
        i = 0;
        while (mas[i].inform == false) {
            i++;
        }
        min = mas[i].birthday;
        imin = i;
        i = i + 1;
        for (i; i < razm; i++) {
            if (mas[i].inform != false) {
                if (mas[i].birthday.year < min.year) {
                    min = mas[i].birthday;
                    imin = i;
                }
                else if (mas[i].birthday.year == min.year) {
                    if (mas[i].birthday.mounth < min.mounth) {
                        min = mas[i].birthday;
                        imin = i;
                    }
                    else if (mas[i].birthday.mounth == min.mounth) {
                        if (mas[i].birthday.day < min.day) {
                            min = mas[i].birthday;
                            imin = i;
                        }
                    }
                }
            }
        }
        cout << "Элемент с минимальным значением даты рождения: " << endl;
        display(mas, imin);
        break;
    case 3:
        while (mas[imin].inform == false) {
            imin++;
        }
        i = imin + 1;
        for (i; i < razm; i++) {
            for (j = 0; j < strlen(mas[i].address); j++) {
                if (mas[i].address[j] < mas[imin].address[j]) {
                    imin = i;
                    break;
                }
                if (mas[i].address[j] > mas[imin].address[j]) {
                    break;
                }
            }
        }
        cout << "Элемент с минимальным значением адреса: " << endl;
        display(mas, imin);
        break;
    }
}

// 8.Сортировка в порядке возрастания заданного поля

void sortvozr(klient* mas) { 
    int vyb, i, j;
    cout << endl << "Выберите строку, по которой хотите произвести сортировку в порядке возрастания" << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < razm - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].name) && k < strlen(mas[i + 1].name); k++) {
                    if (mas[i].name[k] < mas[i + 1].name[k]) {
                        break;
                    }
                    if (mas[i].name[k] > mas[i + 1].name[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;
    case 2:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (i = 0; i < razm - 1; i++) {
                if (mas[i].birthday.year > mas[i + 1].birthday.year) {
                    klient x = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = x;
                }
                else if (mas[i].birthday.year == mas[i + 1].birthday.year)
                {
                    if (mas[i].birthday.mounth > mas[i + 1].birthday.mounth) {
                        klient x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                    }
                    else if (mas[i].birthday.mounth == mas[i + 1].birthday.mounth)
                    {
                        if (mas[i].birthday.day > mas[i + 1].birthday.day) {
                            klient x = mas[i];
                            mas[i] = mas[i + 1];
                            mas[i + 1] = x;
                        }

                    }

                }
            }
        }
        break;
    case 3:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < razm - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].address) && k < strlen(mas[i + 1].address); k++) {
                    if (mas[i].address[k] < mas[i + 1].address[k]) {
                        break;
                    }
                    if (mas[i].address[k] > mas[i + 1].address[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;

    }
}

// 9.Сортировка в порядке убывания

void sortubiv(klient* mas) {  
    int vyb, i, j;
    cout << endl << "Выберите строку, по которой хотите произвести сортировку в порядке убывания" << endl << "1 - Фамилия И.О." << endl << "2 - Дата рождения" << endl << "3 - Адрес" << endl;
    cin >> vyb;
    switch (vyb) {
    case 1:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < razm - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].name) && k < strlen(mas[i + 1].name); k++) {
                    if (mas[i].name[k] > mas[i + 1].name[k]) {
                        break;
                    }
                    if (mas[i].name[k] < mas[i + 1].name[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;
    case 2:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (i = 0; i < razm - 1; i++) {
                if (mas[i].birthday.year < mas[i + 1].birthday.year) {
                    klient x = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = x;
                }
                else if (mas[i].birthday.year == mas[i + 1].birthday.year)
                {
                    if (mas[i].birthday.mounth < mas[i + 1].birthday.mounth) {
                        klient x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                    }
                    else if (mas[i].birthday.mounth == mas[i + 1].birthday.mounth)
                    {
                        if (mas[i].birthday.day < mas[i + 1].birthday.day) {
                            klient x = mas[i];
                            mas[i] = mas[i + 1];
                            mas[i + 1] = x;
                        }

                    }

                }
            }
        }
        break;
    case 3:
        for (j = 1; j < razm; j++) { // Сортировка "пузырьком".
            for (int i = 0; i < razm - 1; i++)
            {
                for (int k = 0; k < strlen(mas[i].address) && k < strlen(mas[i + 1].address); k++) {
                    if (mas[i].address[k] > mas[i + 1].address[k]) {
                        break;
                    }
                    if (mas[i].address[k] < mas[i + 1].address[k]) {
                        klient x;
                        x = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = x;
                        break;
                    }

                }
            }
        }
        break;

    }
}

// 10. чтение файла

void read(klient* mas) {
    FILE* f;
    if ((f = fopen("myFile.txt", "rb")) == NULL) exit(3);
    fread(mas, sizeof(mas[0]), razm, f);
    fclose(f);
}

// 11. запись

void write(klient* mas) {
    FILE* f;
    if ((f = fopen("myFile.txt", "rb")) == NULL) exit(3);
    fwrite(mas, sizeof(mas[0]), razm, f);
    fclose(f);
}



int main()
{
    setlocale(LC_ALL, "Russian");
    FILE* f;	// указатель связанный с файлом
    klient e;	// переменная
    klient mas[razm], mascopy[razm];	//массив
    int vyb, i;
    char ch;
    cout << "1 - Очистка структурной переменной" << endl 
         << "2 - Поиск свободной структурной переменной" << endl
         << "3 - Ввод элементов (полей) структуры с клавиатуры" << endl 
         << "4 - Вывод элементов (полей) структуры на монитор" << endl
         << "5 - Вывод на экран всех заполненных элементов " << endl 
         << "6 - Поиск в массиве структур элемента с заданным значением поля или с наиболее близким к нему по значению " << endl
         << "7 - Поиск в массиве структуры с минимальным значением заданного поля  " << endl 
         << "8 - Сортировка массива структур в порядке возрастания заданного поля " << endl
         << "9 - Сортировка массива структур в порядке убывания заданного поля " << endl 
         << "10 - Заполнить все элементы с клавиатуры" << endl;
    do {
        cout << "Выберите действие(цифру), которое хотите совершить: ";
        cin >> vyb;
        switch (vyb) {
        case 1:
            cout << "Введите элемент, который хотите очистить: ";
            cin >> i;
            clean(mas, i);
            break;
        case 2:
            cout << "Первая свободная структурная переменная - элемент №" << searchclean(mas) << endl;
            break;
        case 3:
            cout << "Введите № элемента, который хотите заполнить: ";
            cin >> i;
            put(mas, i);
            break;
        case 4:
            cout << "Введите № элемента, который хотите вывести на экран: ";
            cin >> i;
            display(mas, i);
            break;
        case 5:
            displayall(mas);
            break;
        case 6:
            search(mas);
            break;
        case 7:
            minimal(mas);
            break;
        case 8:
            sortvozr(mas);
            break;
        case 9:
            sortubiv(mas);
            break;
        case 10:
            for (i = 0; i < razm; i++) {
                put(mas, i);
            }
            break;
        }
        cout << endl << "Продолжить?(y/n) ";
        cin >> ch;
        cout << endl;
    } while (ch != 'n');
    write(mas);
    read(mascopy);
    displayall(mascopy);
}
```