1. Создайте таблицу, содержащую, например, поля ФИО, товар, количество, цена, общая стоимость. 
2. Сделайте из созданной таблицы сводную. Настройте поля Фильтр отчета, Названия столбцов, Названия строк, Значения. 
3. Постройте сводную диаграмму по таблице. 
4. Сделайте из сводной таблицы обычную. 
5. Создайте несколько небольших таблиц с одинаковыми наименованиями полей (столбцов и строк). 
6. Проведите консолидацию данных.





## сводная таблица

<img src="C:\Users\Александр\AppData\Roaming\Typora\typora-user-images\image-20201125151704903.png" alt="image-20201125151704903" style="zoom:50%;" />

## сводная диаграмма

анализ - сервис - диаграмма

<img src="C:\Users\Александр\AppData\Roaming\Typora\typora-user-images\image-20201125151624904.png" alt="image-20201125151624904" style="zoom:50%;" />


## сделать из сводной таблицы обычную таблицу

убрать фильтры
![9f81749c369c68abec883fb8ba4a45ee.png](../../../_resources/9f81749c369c68abec883fb8ba4a45ee.png)
очистить формат
![9e780965b6d7c5b86f464ddc2d7294cf.png](../../../_resources/9e780965b6d7c5b86f464ddc2d7294cf.png)
убрать отсюда все кнопки
![a7eb0985e2965947978f8fb7cb94a8c7.png](../../../_resources/a7eb0985e2965947978f8fb7cb94a8c7.png)