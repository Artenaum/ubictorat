# 16. Use the Web and other resources to compile a list of the software used in your current or future career field. Describe the software and say how you use it.

(Используйте Интернет и другие ресурсы, чтобы составить список программного обеспечения, используемого в вашей текущей или будущей карьере. Опишите программное обеспечение и расскажите, как вы его используете)

$\;$

visual studio code is a text editor for creating and editing program code, writing scripts and applications.

Unity - graphics software for creating games, studying computer graphics, shaders, visualizing physics

Duplicati - open source software designed for automated backup of files and folders in an encrypted form

MySQL workbench - graphical software for creating and editing databases, creating er data models





visual studio code - текстовый редактор для создания и редактирования программного кода, написания скриптов и приложений.

Unity - графическое ПО для создания игр, изучения компьютерной графики, шейдеров, виузализация физики

Duplicati - ПО с открытым исходным кодом, предназначенное для автоматизированного резервного копирования файлов и папок в защифрованном виде

MySQL workbench - графическое ПО для создания и редактирования баз данных, создания er моделей данных

Figma - графическое ПО для веб разработки, необходима для создания интерфейса веб страниц, проработки дизайна, структуры сайта и анимаций

Joplin - графическое текстовое ПО для введения и редактирования заметок в разметке markdown. Необходим в качестве базы знаний и
введения списка дел

LightBulb - программа, необходимая для сохранения зрения и комфортной работы за компьютером. программа изменяет цвет экрана, делая его желтым, тем самым защищая глаза в вечернее время суток от синего света 

Oracle VM VirtualBox - виртуальная машина, эмулирующая работу операционной системы. Предназначенная для тестов, проверки файлов на вирусы и информационной безопасности




$\;$





# 17. As you consider your career goals for the next year, list at least five additional software packages you would like to learn. Explain why they would be helpful.

(Обдумывая свои карьерные цели на следующий год, перечислите как минимум пять дополнительных программных пакетов, которые вы хотели бы изучить. Объясните, почему они могут быть полезны)

$\;$

**PhotoGIMP** - графическое программное обеспечение для редактирования изображения. Бесплатная и открытая замена adobe photoshop 

**Unity** - графическое программное обеспечение для разработки игр и приложений виртуальной/дополненной реальности

**Inscape** - графическое программное обеспечение для создания векторной графики. Бесплатная и открытая альтернатива adobe illustrator

**visual studio** - среда разработки. Необходима для создания скриптов, приложений и редактирования исходного кода

**Blender** - графическое программное обеспечение для создания 3д графики, создания 3д моделей, скульптинга и анимаций

**node js** - среда выполнения javascript кода для реализации серверной части. Нужен для написания сервера и создания веб приложений с базой данных

Cmder - консольное ПО, для замены командной строки виндовс. Необходим для комфортной работы с консольными командами и консольными приложениями

$\;$






# 18. What is software copyright? Speak about commercial software, shareware, freeware. Give definitions, explain why they’re used.

(18. Что такое авторское право на программное обеспечение? Поговорите о коммерческом ПО, условно-бесплатном ПО. Дайте определения, объясните, почему они используются)

$\;$

copyright - restrictions on copying, distributing and reselling software

commercial software is closed source software that is paid for, users cannot edit the code. Users together with the program buy only the right to use it

Software is a closed-source program that can be used for free for a limited period, after this program you need to pay

the program is available free of charge so that you can try out the product before buying


(авторское право - ограничения на копирование, распространение и перепродажу программного обеспечения

коммерческое ПО - это программа с закрытым исходным кодом, которая распространяется платно,пользователи не могут редактировать код. Пользователи вместе с программой покупают лишь право на ее использование

условно-бесплатное ПО - программа с закрытым кодом, которую можно использовать бесплатно в течении ограниченного периода, после этого за программу нужно платить

программу делают условно-бесплатной, чтобы пользователи смогли опробовать продукт перед покупкой)

$\;$





# 19. What is software copyright? Speak about open source and public domain software. Give definitions, explain the difference between them, say why they’re used.

(Что такое авторское право на программное обеспечение? Расскажите о программном обеспечении с открытым исходным кодом и общественном достоянии. Дайте определения, объясните разницу между ними, скажите, почему они используются)



$\;$

(авторское право - это закон, ограничивающий копирование, распространение и перепродажу программного обеспечения

программы с открытым исходным кодом - это исходный код программы, который можно изменять, компилировать и свободно пользоваться программой

программа, которая являетя общественным достоянием распространяется без авторского права, бесплатно и без ограничения, за исключением продажи авторских прав на нее

разница между ними в том, что у программы с открытым кодом есть авторское право и при использовании или модификации программы она должна оставаться с открытым исходным кодом
)



$\;$



# 20. Is software piracy really a crime? Give reasons for your opinion.

(Является ли программное пиратство преступлением? Обоснуйте свое мнение)

$\;$


Digital piracy is illegal copying of software and is undoubtedly a serious crime.

The Business Software Alliance (BSA) estimates that over 800,000 websites sell or distribute software

In many countries, software pirates face civil claims for pay and criminal prosecutions that can lead to jail time and heavy fines

Declining software revenues can also have a direct impact on consumers. When software publishers are forced to cut corners, they tend to cut back on customer service and support.


$\;$

(Цифровое пиратство - это незаконное копирование программного обеспечения и , несомненно, является серьезным преступлением.

По оценкам Business Software Alliance (BSA), более 800 000 веб-сайтов незаконно продают или распространяют программное обеспечение

Во многих странах к пиратам программного обеспечения предъявляются гражданские иски о возмещении денежного ущерба и уголовное преследование, которое может привести к тюремному заключению и крупным штрафам

Снижение доходов от программного обеспечения также может иметь прямое влияние на потребителей. Когда издатели программного обеспечения вынуждены срезать углы, они, как правило, сокращают объем обслуживания клиентов и технической поддержки)

