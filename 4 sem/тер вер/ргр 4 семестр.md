[TOC]

# 9



![996132ef4c739591caa6cae2a9b6d6e9.png](../../../_resources/996132ef4c739591caa6cae2a9b6d6e9.png)



$y_1=2\sqrt{0} = 0$
$y_2=2\sqrt{1}=2$
$y_3=2\sqrt{4}=4$


| Y | 0 | 2 | 4 |
| - | - | - | - |
| p | 0.1 | 0.4 | 0.5 |


<br>

$Z=X+Y=X+2\sqrt{X}$
$z_1=0+2\sqrt{0}=0$
$z_2=1+2\sqrt{1}=3$
$z_3=4+2\sqrt{4}=8$



<br>

|Z|0|3|8|
|-|-|-|-|
|p|0.1|0.4|0.5|



---


# 10 

![da8b71e37cb05415698714e51158803a.png](../../../_resources/da8b71e37cb05415698714e51158803a.png)


0 1 2 3 4 5 6 7 8 9

нечетные - 1 3 5 7 9





$x_1 = 0$
$x_2 = 1$
$x_3 = 2$
$x_4 = 3$
$x_5 = 4$

$P(X=0)=C^0_4*(\frac12)^0*(\frac12)^4=\frac{1}{16}$
$P(X=1)=C^1_4*(\frac12)^1*(\frac12)^3=\frac4{16}$
$P(X=2)=C^2_4*(\frac12)^2*(\frac12)^2=\frac6{16}$
$P(X=3)=C^3_4*(\frac12)^3*(\frac12)^1=\frac4{16}$
$P(X=4)=C^4_4*(\frac12)^4*(\frac12)^0=\frac1{16}$

<br>

| X | 0 | 1 | 2 | 3 | 0 |
| - | - | - | - | - | - |
| p | $\frac1{16}$ | $\frac4{16}$ | $\frac6{16}$ | $\frac4{16}$ | $\frac1{16}$ |



$MX=np=4*\frac12=2$

$DX=npq=4*\frac12*\frac12=1$

---


# 11







![804e42aaa6182da560f7edc6e6877d40.png](../../../_resources/804e42aaa6182da560f7edc6e6877d40.png)

$F(x)=\begin{cases} 0,\  x\le -\frac \pi 2,\\ cos(x),\ -\frac \pi 2 < x \le 0,\\ 1,\ x>0 \end{cases} \bigg[\frac \pi 4; \frac \pi 4 \bigg)$

<br>

$f(x) = \begin{cases} 0,\ x < -\frac \pi 2,\\ -sin(x),\ -\frac \pi 2 < x <0,\\ 0,\ x > 0 \end{cases}$

<br>

$P(-\frac \pi 4 \le X < \frac \pi 4) = F(\frac \pi 4) - F(-\frac \pi 4) =\\ = 0 - cos(-\frac \pi 4) = 0 - \frac {\sqrt{2}} = -\frac 1 {\sqrt{2}}$

<br>

$MX = \int^{+\infty}_{-\infty}xf(x)dx = \int^{-\frac \pi 2}_{-\infty}0dx + \int^{0}_{-\frac \pi 2}x(sin(x))dx + \int^{+\infty}_{0}0dx=\\= \int^{0}_{-\frac \pi 2}x(-sin(x))dx = xcos(x) - sin(x) \bigg|^{0}_{-\frac \pi 2} = -1$

<br>


---

# 12



![ac4dc04d869ffd1774de5b91b65ed5a5.png](../../../_resources/ac4dc04d869ffd1774de5b91b65ed5a5.png)

$f(x) = \begin{cases}0,\ x<1,\\\frac14x^3+c,\ 1<x<2,\\0,\ x>2\end{cases}[1.5; 2.5)$

<br>

$c = \int^{2}_{1}\frac14x^3+cdx=1 \\ \frac{15}{16}+c=1 \\  c = \frac{1}{16}$

<br>

$F(x): x<1:\ F(x) = \int^x_{-\infty}0dt = 0\\1<x<2:\ F(x)=\int^{0}_{-\infty}0dt+\int^{x}_{1}\frac14t^3+\frac{1}{16}dt=\\= \frac{1}{16}(x^4+x-2) \\ x>2:\ F(x)=\int^{0}_{-\infty}0dt+\int^{2}_{1}\frac14t^3+\frac{1}{16}dt+\int^x_20dt=\\= 1\\ F(x) = \begin{cases}0,\ x\le0,\\\frac{1}{16}(x^4+x-2),\ 1<x\le2,\\1,\ x>2\end{cases}$

<br>

$P(1.5\le X < 2.5) = F(2.5) - F(1.5) = 1-\frac{1}{16}(1.5^4+1.5-2) = \frac{183}{256}$

<br>

$DX = \int^{+\infty}_{-\infty}x^2f(x)dx-(MX)^2$

$MX = \int^{+\infty}_{-\infty}xf(x)dx=\int^{1}_{-\infty}0xdx+\int^{2}_{1}(\frac14x^3+\frac{1}{16})xdx+
+\int^{+\infty}_{2}0xdx = \frac{263}{160}$

$DX = \int^{1}_{-\infty}0x^2dx+\int^{2}_{1}(\frac14x^3+\frac{1}{16})x^2dx + \int^{+\infty}_{2}0x^2dx-(\frac{263}{160})^2=\frac{133}{48}-(\frac{263}{160})^2 = \frac{5293}{76800}$




<style> 
	hr {
		border: 2px solid green;
	}
</style>


