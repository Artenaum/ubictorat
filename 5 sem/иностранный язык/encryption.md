1. Hello, my name is Alexander, today i will talk about cryptography. At currently cryptography is one of the the most important technology for world. Finanse industry, storage personal files, storage corporate secrets, transaction security need a crypt systems. 

---

2. Encryption - reversible transformation of information in order to hide it from unauthorized persons, with the provision, at the same time, authorized users of access to it . in the modern world, encryption, information protection, security of voice, video and text communications, and so on are ubiquitous. without encryption, neither the Internet nor humanity could exist

---

3. today there are many encryption protocols. the essence of encryption is to generate a character set based on a password. there is the AES 256 protocol, an international standard that provides excellent data security and is recognized, in particular, by the US government. AES-256 encryption is virtually indecipherable, making it the most reliable encryption standard.


---

4. web protocols - protocols that provide remote access over the Internet. I will give ssh protocol as an example. It is used to securely connect to the server. It is used with different encryption protocols and also works with digital signature algorithms.

---

4. For example. SSH allows remote control of the operating system and tunneling of network protocol connections TCP connections (for example, for application layer transmission)

---

5. the advantages are obvious - data reliability. only you, knowing the password, can use the data. however, there are still drawbacks. firstly, if you forget your password, you will lose access to the data. Secondly, the password must be complex, at least 12 characters. And a few of these passwords are almost impossible to remember. Therefore, you will have to use password managers, which is not convenient for everyone.

---

6. Finally, cryptography is a necessary tool for the operation of the modern Internet. Anyone should know how to secure their data. Finally, encryption is a necessary tool for the modern Internet to work. despite its drawbacks, encryption is indispensable. each person decides for himself, either safely or conveniently















