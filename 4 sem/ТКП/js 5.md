# задание 003

Создать программную модель стека, используя объектно-ориентированный подход

```js
    let output = document.getElementById('output');
    function Stack() {
        this.dataStore = [];
        this.top = 0;
        this.push = push;
        this.pop = pop;
        this.result = result;
    }
    function push(element) {
        this.dataStore[this.top++] = element;
    }
    function pop() {
        return this.dataStore[--this.top];
    }
    function clear() {
        this.top = 0;
    }
    function result() { 
        for(var i = 0; i < this.top; i++) {
            output.innerHTML +=  this.dataStore[i]+ " ";
        }
        output.innerHTML += '<br>';
    }
    var s = new Stack();
    s.push(1);
    s.push(2);
    s.push(3);
    s.result();
    s.pop();
    s.push(4);
    s.result();
    s.pop();
    s.result();
```

![4313114505268e6c3d0fc3c80c8375cf.png](../../../_resources/4313114505268e6c3d0fc3c80c8375cf.png)

# задание 004

Создать программную модель циклической очереди, используя объектно-ориентированный подход

```js
    let output = document.getElementById('output');
    function Queue() {
        this.dataStore = [];
        this.top = 0; 
        this.push = push;
        this.shift = shift;
        this.result = result;
    }
        
    function push(element) {
        this.dataStore[this.top++] = element;
    } 
            
    function shift() {
        delete this.dataStore[0];
        for(var i=0; i < this.top + 1;i++) {
            this.dataStore[i] = this.dataStore[i+1];
            }
        this.top-=1;
    }
    
    function result() {
        for (var i=0; i < this.top; i++) {
            output.innerHTML +=  this.dataStore[i] + " ";
        }
        output.innerHTML +=  "<br>";
    }

var s = new Queue();
s.push(1);
s.push(2);
s.push(3);
s.result();
s.shift();
s.push(4);
s.result();
s.shift();
s.result();
```

![8839e1db407fd0004ec4f0ba47e0a980.png](../../../_resources/8839e1db407fd0004ec4f0ba47e0a980.png)

# задание 005

Создать программную модель двусвязного списка, используя объектно-ориентированный подход

```js
let output = document.getElementById('output');

function Node(value) {
    this.data = value;
    this.previous = null;
    this.next = null;
}

function DoublyList() {
    this._length = 0;
    this.head = null;
    this.tail = null;
}

DoublyList.prototype.add = function(value) {
    var node = new Node(value);

    if (this._length) {
        this.tail.next = node;
        node.previous = this.tail;
        this.tail = node;
    } else {
        this.head = node;
        this.tail = node;
    }

    this._length++;

    return node;
};

DoublyList.prototype.searchNodeAt = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 1,
        message = {failure: 'Failure: non-existent node in this list.'};

    // 1-ый случай: неверная позиция
    if (length === 0 || position < 1 || position > length) {
        throw new Error(message.failure);
    }

    // 2-ой случай: верная позиция
    while (count < position) {
        currentNode = currentNode.next;
        count++;
    }

    return currentNode;
};

DoublyList.prototype.remove = function(position) {
    var currentNode = this.head,
        length = this._length,
        count = 1,
        message = {failure: 'Failure: non-existent node in this list.'},
        beforeNodeToDelete = null,
        nodeToDelete = null,
        deletedNode = null;

    // 1-ый случай: неверная позиция
    if (length === 0 || position < 1 || position > length) {
        throw new Error(message.failure);
    }

    // 2-ой случай: первый узел удален
    if (position === 1) {
        this.head = currentNode.next;

        // 2-ой случай: существует второй узел
        if (!this.head) {
            this.head.previous = null;
        // 2-ой случай: второго узла не существует
        } else {
            this.tail = null;
        }

    // 3-ий случай: последний узел удален
    } else if (position === this._length) {
        this.tail = this.tail.previous;
        this.tail.next = null;
    // 4-ый случай: средний узел удален
    } else {
        while (count < position) {
            currentNode = currentNode.next;
            count++;
        }

        beforeNodeToDelete = currentNode.previous;
        nodeToDelete = currentNode;
        afterNodeToDelete = currentNode.next;

        beforeNodeToDelete.next = afterNodeToDelete;
        afterNodeToDelete.previous = beforeNodeToDelete;
        deletedNode = nodeToDelete;
        nodeToDelete = null;
    }

    this._length--;

    return message.success;
};

DoublyList.prototype.display = function () {
    var currNode = this.head;
    while (currNode != null) {
        output.innerHTML += currNode.data + ' ';
        currNode = currNode.next;
    } 
    output.innerHTML += '<br>';
}


let list = new DoublyList();
list.add(1);
list.add(2);
list.add(857);
list.display();
list.remove(2);
list.display();
```

![efd57a38137b543996194b09120200a3.png](../../../_resources/efd57a38137b543996194b09120200a3.png)