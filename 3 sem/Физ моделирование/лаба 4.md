# видео

https://youtu.be/RHx4xOpCoR8

# задание 1

Объе кт находится в начальной точке. Через промежуток времени tначинает движение по винтовой линии радиуса Rвдоль одной из осей координат. Движение с постоянной скоростью. Исходные данные: скорость, время. Определить путь, пройденный телом

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_1 : MonoBehaviour
{
    Vector3 newPosition;
    float Revs, arcLength;
    float timer, path;
    public float speed, 
    R, 
    height, 
    time;

    void FixedUpdate()
    {
        if(R > 0 && speed > 0 && time > 0)
        {
            timer += Time.deltaTime;
        }

        if(timer >= time)
        {
            arcLength = Mathf.Sqrt(Mathf.Pow(2 * R * Mathf.PI, 2) + Mathf.Pow(height, 2));
            Revs = (timer - time) / (2 * Mathf.PI * R / speed);
            path = arcLength * Revs;

            newPosition.x = R * Mathf.Cos((timer - time) * speed / R);
            newPosition.y = Revs * height;
            newPosition.z = R * Mathf.Sin((timer - time) * speed / R);

            transform.position = newPosition;
            print("Path: " + path);
        }
        else
        {
            newPosition.x = R;
            transform.position = newPosition;
        }
    }
}

```



# задание 2

Объект находится в начальной точке. Через промежуток времени tначинает движение по винтовой линии радиуса Rвдоль одной из осей координат. Движение с постоянным ускорением. Исходные данные: скорость, ускорение, время. Определить путь, пройденный телом, координаты через время t1от начала движения

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_2 : MonoBehaviour
{
    Vector3 NewPos, xyz;
    bool flag = true;
    float Revs, arcLength;
    float timer, speed, path;
    public float velocity, time1, time2, height, radius, a;
     
    void FixedUpdate()
    {
        if(radius > 0 && velocity > 0 && time1 > 0 && time2 > 0)
        {
            timer += Time.deltaTime;
        }

        if(timer >= time1)
        {
            speed = velocity + a * (timer - time1);
            arcLength = Mathf.Sqrt(Mathf.Pow(2 * radius * Mathf.PI, 2) + Mathf.Pow(height, 2));
            Revs = (timer - time1) / (2 * Mathf.PI * radius / speed);
            path = arcLength * Revs + a * Mathf.Pow(time1, 2) / 2;

            NewPos.x = radius * Mathf.Cos((timer - time1) * speed / radius);
            NewPos.y = Revs * height;
            NewPos.z = radius * Mathf.Sin((timer - time1) * speed / radius);

            transform.position = NewPos;

            if(((int)(timer - time1) == (int)(time2 - time1)) && flag)
            {
                xyz = NewPos;
                flag = false;
                print("Path: " + path + "; Coords: " + xyz);
            }
        }
        else
        {
            NewPos.x = radius;
            transform.position = NewPos;
        }
    }
}

```



# задание 3

Объек  т находится в начальной точке. Через промежуток времени t начинает движение по винтовой линии радиуса R вдоль одной из осей координат. Движение с ускорением, определяемом по закону вида a=A+B*t. Исходные данные: скорость, ускорение, время. Определить путь, пройденный телом

## скрипт 

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_3 : MonoBehaviour
{
    Vector3 newPosition;
    float Revs, arcLength;
    float timer, speed, a, path;
    public float A, B, Vel, R, height, time;
    
    void FixedUpdate()
    {
        if(R > 0 && Vel > 0 && time > 0)
        {
            timer += Time.deltaTime;
        }

        if(timer >= time)
        {
            a = A + B * (timer - time);
            speed = Vel + a * (timer - time);
            arcLength = Mathf.Sqrt(Mathf.Pow(2 * R * Mathf.PI, 2) + Mathf.Pow(height, 2));
            Revs = (timer - time) / (2 * Mathf.PI * R / speed);
            path = arcLength * Revs;

            newPosition.x = R * Mathf.Cos((timer - time) * speed / R);
            newPosition.y = Revs * height;
            newPosition.z = R * Mathf.Sin((timer - time) * speed / R);

            transform.position = newPosition;
            print("Path: " + path);
        }
        else
        {
            newPosition.x = R;
            transform.position = newPosition;
        }
    }
}

```

