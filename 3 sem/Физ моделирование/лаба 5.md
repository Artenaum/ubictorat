# задание 1

Объект бросили со скорость v c высоты h. Определитьвремя и дальность полёта

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_1 : MonoBehaviour
{
   public float speed, height;
    float distance, counter;
    Vector3 newPosition;
    const float g = 9.81f;
    bool isFlying = true;

    void FixedUpdate()
    {
        counter += Time.deltaTime;

        if (isFlying)
        {
            newPosition.x = transform.position.x;
            newPosition.y = height - g * Mathf.Pow(counter, 2) / 2;
            newPosition.z = speed * counter;

            transform.position = newPosition;

            distance = transform.position.z;

            print("Distance: " + distance + "; Time: " + counter);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        print("Distance: " + distance + "; Time: " + counter);
        isFlying = false;
        speed = 0;
        height = 0;
    }
}

```



# задание 2

Объекту   , покоящемуся на высоте h, в момент времени tоднократно придали ускорение A, направленное под углом к горизонту α. Определить время полёта, среднюю скорость в полёте, скорость в момент приземления,дальность полета объекта

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_2 : MonoBehaviour
{
   public float A, height, time, alphaDegrees;
    float distance, alpha, counter, meanSpeed, speed, distancePerTime, speedZ, speedY, AZ, AY, path;
    Vector3 newPosition, oldPosition;
    const float g = 9.81f;
    bool isFlying = true;

    void Start()
    {
        alpha = Mathf.PI / 180 * alphaDegrees;
        speedZ = A * Mathf.Cos(alpha);
        speedY = A * Mathf.Sin(alpha);
        AZ = 0;
        AY = -g;
    }

    void FixedUpdate()
    {
        counter += Time.deltaTime;

        if (counter >= time)
        {
            if (isFlying)
            {
                newPosition.x = transform.position.x;
                newPosition.y = height + (speedY * (counter - time)) + (AY * Mathf.Pow(counter - time, 2) / 2) + (A * Mathf.Pow(counter - time, 2) / 2 );
                newPosition.z = speedZ * (counter - time) + (A * Mathf.Pow(counter - time, 2) / 2);
                oldPosition = transform.position;
                transform.position = newPosition;

                path += Mathf.Sqrt(Mathf.Pow(newPosition.z - oldPosition.z, 2) + Mathf.Pow(newPosition.y - oldPosition.y, 2));

                distancePerTime = newPosition.z - oldPosition.z;
                distance = transform.position.z;
                speed = distancePerTime / (counter - time);

                print("Distance: " + distance + "; Time: " + (counter - time));
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        meanSpeed = distance / (counter - time);

        print("Distance: " + distance + "; Time: " + (counter - time) + ";\nSpeed: " + speed + "; Mean Speed: " + meanSpeed + "; Path: " + path);
        isFlying = false;
        A = 0;
        height = 0;
    }
}

```



# задание 3

Объект, который покоился на высоте h, в момент времени tполучил постоянное ускорение A, направленное под углом αк горизонту. Определить время полёта, среднюю скорость полёта, скорость в момент приземления, дальность полета и путь пройденный объектом

## скрипт

```c#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script_3 : MonoBehaviour
{
    public float A, height, time, alphaDegrees;
    float distance, alpha, counter, meanSpeed, speed, distancePerTime, speedZ, speedY, AZ, AY, path;
    Vector3 newPosition, oldPosition;
    const float g = 9.81f;
    bool isFlying = true;

    void Start()
    {
        alpha = Mathf.PI / 180 * alphaDegrees;
        speedZ = A * Mathf.Cos(alpha);
        speedY = A * Mathf.Sin(alpha);
        AZ = 0;
        AY = -g;
    }

    void FixedUpdate()
    {
        counter += Time.deltaTime;

        if (counter >= time)
        {
            if (isFlying)
            {
                newPosition.x = transform.position.x;
                newPosition.y = height + (speedY * (counter - time)) + (AY * Mathf.Pow(counter - time, 2) / 2) + (A * Mathf.Pow(counter - time, 2) / 2 );
                newPosition.z = speedZ * (counter - time) + (A * Mathf.Pow(counter - time, 2) / 2);
                oldPosition = transform.position;
                transform.position = newPosition;

                path += Mathf.Sqrt(Mathf.Pow(newPosition.z - oldPosition.z, 2) + Mathf.Pow(newPosition.y - oldPosition.y, 2));

                distancePerTime = newPosition.z - oldPosition.z;
                distance = transform.position.z;
                speed = distancePerTime / (counter - time);

                print("Distance: " + distance + "; Time: " + (counter - time));
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        meanSpeed = distance / (counter - time);

        print("Distance: " + distance + "; Time: " + (counter - time) + ";\nSpeed: " + speed + "; Mean Speed: " + meanSpeed + "; Path: " + path);
        isFlying = false;
        A = 0;
        height = 0;
    }
}

```

