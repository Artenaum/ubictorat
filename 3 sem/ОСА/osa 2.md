[TOC]

# Цель:

Получить практические навыки в использовании

структурных типов данных в языке С



# Постановка задачи:

Разработать программу ведения и использование базы данных с использованием структурных переменных и сохранением данных в плоском (без использования форматирования) файле



# Шаблон структуры

- структура дата
- структура книга
- очистка данных переменной
- поиск свободной переменной
- ввод структурной переменной
- вывод выбранной переменной на экран
- вывод на экран всех заполненных элементов
- поиск в массиве данного значения или ближайшего к нему
- поиск индекса элемента с минимальным введенным значением
- сортировка массива в порядке возрастания
- сортировка массива в порядке убывания
- запись данных в файл
- чтение файла

- главная функция

# описание 

1. EmptyArray() - очистка данных переменной

2. FindFirstEmpty() - поиск свободной переменной

3. InputElem() - ввод структурной переменной

4. OutputElem() - вывод выбранной переменной на экран

5. OutputAllFull() - вывод на экран всех заполненных элементов

6. SearchExact() - поиск в массиве данного значения или ближайшего к нему

7. SearchMin() - поиск индекса элемента с минимальным введенным значением

8. SortMinToMax() - сортировка массива в порядке возрастания

9. SortMaxToMin() - сортировка массива в порядке убывания

10. ReadFile() - запись данных в файл

11. WriteFile() - чтение файла

    

# лист функций

## структура студент

name - имя

familia - фамилия

patronymic - отчество

## структура книга

book_number - номер зачетной книжки

facultet - факультет

group - группа 

## 1 очистка данных переменной

```c++
void EmptyArray(int index, book* arr)
{
    arr[index].group = true;
}
```


## 2 поиск свободной переменной

```c++
int FindFirstEmpty(book* arr)
{
    int index = 0;
    while (!arr[index].group && index < 5)
    {
        index++;
    }
    index--;
    return index;
}
```

изначально индекс равен нулю.  

## 3 ввод структурной переменной

```c++
void InputElem(int index, book* arr)
{
    student tempdata;
    string tempName;
    int tempbook_number, tempfacultet;

    cout << "Имя: ";
    cin >> tempName;

    cout << "Фамилия: ";
    cin >> tempdata.familia;

    cout << "Отчество: ";
    cin >> tempdata.otchestvo;

    cout << "Номер зачетной книжки: ";
    cin >> tempbook_number;
    
    cout << "Факультет: ";
    cin >> tempfacultet;

    cout << '\n';

    arr[index].bookName = tempName;
    arr[index].data = tempdata;
    arr[index].book_number = tempbook_number;
    arr[index].facultet = tempfacultet;
    arr[index].group = false;
}
```

## 4 вывод выбранной переменной на экран

```c++
void OutputElem(int index, book* arr)
{
    cout << "Ввод: " << arr[index].bookName << endl;
    cout << "Зачетная книжка: " << arr[index].book_number << endl;
    cout << "факультет: " << arr[index].facultet << endl;
    cout << '\n';
}
```

## 5 вывод на экран всех заполненных элементов

```c++
void OutputAllFull(book* arr)
{
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            OutputElem(i, arr);
        }
    }
}
```

## 6 поиск в массиве данного значения или ближайшего к нему

```c++
void SearchExact(book* arr)
{
    string s;
    cout << "Ввод: ";
    cin >> s;

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            if (arr[i].bookName == s || arr[i].book_number == atoi(s.c_str()) || arr[i].facultet == atoi(s.c_str()))
            {
                OutputElem(i, arr);
            }
            else cout << "Ничего нет\n";
        }
    }
}
```

## 7 поиск индекса элемента с минимальным введенным значением

```c++
void SearchMin(book* arr, int fieldNum)
{
    int min;

    if (fieldNum == 1)
    {
        min = arr[0].book_number;
    }
    else if (fieldNum == 2)
    {
        min = arr[0].facultet;
    }

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            if (fieldNum == 1)
            {
                if (arr[i].book_number < min)
                {
                    min = arr[i].book_number;
                }
            }
            else if (fieldNum == 2)
            {
                if (arr[i].facultet < min)
                {
                    min = arr[i].facultet;
                }
            }
        }
    }

    for (int i = 0; i < 5; i++)
    {
        if (fieldNum == 1)
        {
            if (arr[i].book_number == min)
            {
                OutputElem(i, arr);
            }
        }
        else if (fieldNum == 2)
        {
            if (arr[i].facultet == min)
            {
                OutputElem(i, arr);
            }
        }
    }
}
```

## 8 сортировка массива в порядке возрастания

```c++
void SortMinToMax(book* arr, int fieldNum)
{
    switch (fieldNum)
    {
        case 1:
        {
            sort(arr, arr+5, compNameMinToMax);
            OutputAllFull(arr);
            break;
        }
        case 2:
        {
            sort(arr, arr+5, compbook_numberMinToMax);
            OutputAllFull(arr);
            break;
        }
        case 3:
        {
            sort(arr, arr+5, compfacultetMinToMax);
            OutputAllFull(arr);
            break;
        }
        default:
        {
            cout << "Неправильный ввод";
            break;
        }
    }
}
```

## 9 сортировка массива в порядке убывания

```c++
void SortMaxToMin(book* arr, int fieldNum)
{
    switch (fieldNum)
    {
        case 1:
        {
            sort(arr, arr+5, compNameMaxToMin);
            OutputAllFull(arr);
            break;
        }
        case 2:
        {
            sort(arr, arr+5, compbook_numberMaxToMin);
            OutputAllFull(arr);
            break;
        }
        case 3:
        {
            sort(arr, arr+5, compfacultetMaxToMin);
            OutputAllFull(arr);
            break;
        }
        default:
        {
            cout << "Неправильный ввод";
            break;
        }
    }
}
```



## 10 запись данных в файл

```c++
void ReadFile()
{
    book arr[5];
    fopen(&fp, "database.txt", "r");
    for (int i = 0; i < 5; i++)
    {
        fread(&arr[i], sizeof(book), 1, fp);
    }
    fclose(fp);
    OutputAllFull(arr);
}
```


![8f491d239258c423a23124a7aa4b4981.png](../../../_resources/8f491d239258c423a23124a7aa4b4981.png)





## 11 чтение файла

```c++
void WriteFile(book* arr)
{
    fopen(&fp, "database.txt", "w");
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            fwrite(&arr[i], sizeof(book), 1, fp);
        }
    }
    fclose(fp);
}
```


![d3b30ce8588f90c95af29abb9518a782.png](../../../_resources/d3b30ce8588f90c95af29abb9518a782.png)

# Код программы

```c++


#include <iostream>
#include <algorithm>
#include <stdio.h>
using namespace std;
FILE* fp;




struct student 
{
    int name;
    int familia; 
    int otchestvo; 
} ;



struct book
{
    string bookName;
    student data;
    int book_number; 
    int facultet; 
    bool group; 
} ;


//

void EmptyArray(int index, book* arr)
{
    arr[index].group = true;
}

//

int FindFirstEmpty(book* arr)
{
    int index = 0;
    while (!arr[index].group && index < 5)
    {
        index++;
    }
    index--;
    return index;
}

//

void InputElem(int index, book* arr)
{
    student tempdata;
    string tempName;
    int tempbook_number, tempfacultet;

    cout << "Имя: ";
    cin >> tempName;

    cout << "Date name: ";
    cin >> tempdata.name;
    cout << "Date familia: ";
    cin >> tempdata.familia;
    cout << "Date otchestvo: ";
    cin >> tempdata.otchestvo;


    cout << "book_number: ";
    cin >> tempbook_number;
    cout << "facultet: ";
    cin >> tempfacultet;

    cout << '\n';

    arr[index].bookName = tempName;
    arr[index].data = tempdata;
    arr[index].book_number = tempbook_number;
    arr[index].facultet = tempfacultet;
    arr[index].group = false;
}

// 

void OutputElem(int index, book* arr)
{
    cout << "Name: " << arr[index].bookName << endl;
    cout << "Date: " << arr[index].data.familia << ':'
         << arr[index].data.name << ' '
         << arr[index].data.otchestvo << '.' << endl;
    cout << "book_number: " << arr[index].book_number << endl;
    cout << "facultet: " << arr[index].facultet << endl;
    cout << '\n';
}

//

void OutputAllFull(book* arr)
{
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            OutputElem(i, arr);
        }
    }
}

//

void SearchExact(book* arr)
{
    string s;
    cout << "Enter: ";
    cin >> s;

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            if (arr[i].bookName == s || arr[i].book_number == atoi(s.c_str()) || arr[i].facultet == atoi(s.c_str()))
            {
                OutputElem(i, arr);
            }
            else cout << "Nothing found\n";
        }
    }
}

//

void SearchMin(book* arr, int fieldNum)
{
    int min;

    if (fieldNum == 1)
    {
        min = arr[0].book_number;
    }
    else if (fieldNum == 2)
    {
        min = arr[0].facultet;
    }

    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            if (fieldNum == 1)
            {
                if (arr[i].book_number < min)
                {
                    min = arr[i].book_number;
                }
            }
            else if (fieldNum == 2)
            {
                if (arr[i].facultet < min)
                {
                    min = arr[i].facultet;
                }
            }
        }
    }

    for (int i = 0; i < 5; i++)
    {
        if (fieldNum == 1)
        {
            if (arr[i].book_number == min)
            {
                OutputElem(i, arr);
            }
        }
        else if (fieldNum == 2)
        {
            if (arr[i].facultet == min)
            {
                OutputElem(i, arr);
            }
        }
    }
}



bool compNameMinToMax(book a, book b)
{
    return a.bookName < b.bookName;
}

bool compbook_numberMinToMax(book a, book b)
{
    return a.book_number < b.book_number;
}

bool compfacultetMinToMax(book a, book b)
{
    return a.facultet < b.facultet;
}

bool compNameMaxToMin(book a, book b)
{
    return a.bookName > b.bookName;
}

bool compbook_numberMaxToMin(book a, book b)
{
    return a.book_number > b.book_number;
}

bool compfacultetMaxToMin(book a, book b)
{
    return a.facultet > b.facultet;
}

//

void SortMinToMax(book* arr, int fieldNum)
{
    switch (fieldNum)
    {
        case 1:
        {
            sort(arr, arr+5, compNameMinToMax);
            OutputAllFull(arr);
            break;
        }
        case 2:
        {
            sort(arr, arr+5, compbook_numberMinToMax);
            OutputAllFull(arr);
            break;
        }
        case 3:
        {
            sort(arr, arr+5, compfacultetMinToMax);
            OutputAllFull(arr);
            break;
        }
        default:
        {
            cout << "Error: incorrect input!";
            break;
        }
    }
}

//

void SortMaxToMin(book* arr, int fieldNum)
{
    switch (fieldNum)
    {
        case 1:
        {
            sort(arr, arr+5, compNameMaxToMin);
            OutputAllFull(arr);
            break;
        }
        case 2:
        {
            sort(arr, arr+5, compbook_numberMaxToMin);
            OutputAllFull(arr);
            break;
        }
        case 3:
        {
            sort(arr, arr+5, compfacultetMaxToMin);
            OutputAllFull(arr);
            break;
        }
        default:
        {
            cout << "Error: incorrect input!";
            break;
        }
    }
}




void ReadFile()
{
    book arr[5];
    fopen_s(&fp, "database.txt", "r");
    for (int i = 0; i < 5; i++)
    {
        fread(&arr[i], sizeof(book), 1, fp);
    }
    fclose(fp);
    OutputAllFull(arr);
}




void WriteFile(book* arr)
{
    fopen_s(&fp, "database.txt", "w");
    for (int i = 0; i < 5; i++)
    {
        if (!arr[i].group)
        {
            fwrite(&arr[i], sizeof(book), 1, fp);
        }
    }
    fclose(fp);
}

int main()
{
    setlocale(LC_ALL, "Russian");
    FILE *f;
    book m[5];
    int selection;
    int index;
    bool exit = false;

    do
    {
        cout << '\n';
        cout << "1: Ввести параметры" << endl;
        cout << "2: Show book" << endl;
        cout << "3: Show all books" << endl;
        cout << "4: Show first empty" << endl;
        cout << "5: Mark as empty" << endl;
        cout << "6: Search exact" << endl;
        cout << "7: Search min" << endl;
        cout << "8: Sort MinToMax" << endl;
        cout << "9: Sort MaxToMin" << endl;
        cout << "10: Safe as file" << endl;
        cout << "11: Open file" << endl;
        cout << "12: Exit program" << endl;

        cin >> selection;
        cout << '\n';

        switch (selection)
        {
            case 1:
            {
                cout << "Enter index to fill" << endl;
                cin >> index;
                InputElem(index, m);
                break;
            }
            case 2:
            {
                cout << "Enter index to show" << endl;
                cin >> index;
                OutputElem(index, m);
                break;
            }
            case 3:
            {
                OutputAllFull(m);
                break;
            }
            case 4:
            {
                FindFirstEmpty(m);
                break;
            }
            case 5:
            {
                cout << "Enter index to mark as empty" << endl;
                cin >> index;
                EmptyArray(index, m);
                break;
            }
            case 6:
            {
                SearchExact(m);
                break;
            }
            case 7:
            {
                int field;
                cout << "book_number(1), facultet(2): ";
                cin >> field;
                SearchMin(m, field);
                break;
            }
            case 8:
            {
                int field;
                cout << "Name(1), book_number(2), facultet(3): ";
                cin >> field;
                SortMinToMax(m, field);
                break;
            }
            case 9:
            {
                int field;
                cout << "Name(1), book_number(2), facultet(3): ";
                cin >> field;
                SortMaxToMin(m, field);
                break;
            }
            case 10:
            {
                 WriteFile(m);
                break;
            }
            case 11:
            {
                 ReadFile();
                break;
            }
            case 12:
            {
                exit = true;
                break;
            }
            default:
            {
                cout << "Error: incorrect input!" << endl;
                break;
            }
        }
    } while (!exit);
    return 0;
}
```















