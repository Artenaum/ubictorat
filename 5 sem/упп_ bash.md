# задание 1

чтобы убрать лишние строки выполнялась команда sed '/^$/d'  test.txt > out.txt

чтобы сделать буквы большими выполнялась команда tr "a-z" "A-Z" < out.txt

![40acc9fa50d956eef357c335347580e1.png](../../_resources/40acc9fa50d956eef357c335347580e1.png)

# задание 2

mkdir -p tmp/201{0..7}/{01..12} && for year in {2010..2017}; do for month in {01..12}; do for file_number in {001..003}; do echo "Файл "$file_number > ~/tmp/$year/$month/$file_number.txt; done; done; done

![6227416b73f5e5c75cc7c2af270a4417.png](../../_resources/6227416b73f5e5c75cc7c2af270a4417.png)

# задание 3

![5a2697b3ed21a57cb0d0be2225e91315.png](../../_resources/5a2697b3ed21a57cb0d0be2225e91315.png)

# задание 4

![b1ac2ed814ea6f5ece3d6f63bc6c71d8.png](../../_resources/b1ac2ed814ea6f5ece3d6f63bc6c71d8.png)

# задание 5
![1d43e6f6640bd7102d0e819a61ae9ee1.png](../../_resources/1d43e6f6640bd7102d0e819a61ae9ee1.png)



